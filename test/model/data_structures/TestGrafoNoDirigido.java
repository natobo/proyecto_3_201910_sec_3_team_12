package model.data_structures;

import junit.framework.TestCase;

/**
 * Prueba la implementacion de grafoNoDirigido, ver imagen de la carpeta docs para observar los grafo Integer, el grafo Strings es uno paralelo pero con valores alfabeticos
 */
public class TestGrafoNoDirigido extends TestCase 
{
	/**
	 *Grafo compuesto por nodos de Integers, valores Integers y arcos con informacion integer. 
	 */
	private GrafoNoDirigido<Integer, Integer, Integer> grafoIntegers;
	/**
	 *Grafo compuesto por nodos de Integers, valores Integers y arcos con informacion integer. 
	 */
	private GrafoNoDirigido<String, String, String> grafoStrings;
	/**
	 * Crea el escenario para las pruebas del test
	 */
	public void setUp() 
	{
		try
		{
			//Crea el grafo de enteros
			grafoIntegers=new GrafoNoDirigido<Integer, Integer, Integer>();
			//Crea el grafo de strings
			grafoStrings=new GrafoNoDirigido<String, String, String>();
			//A�ade los vertices a ambos grafos de prueba
			a�adirVertices();
			//A�ade los Arcos a ambos grafos de prueba 
			a�adirArcos();
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			fail("Los grafos no se han podido inicializar");
		}
	}
	/*
	 * A�ade todos los vertices del escenario de prueba
	 */
	private void a�adirVertices()
	{
		//A�ade los elementos al grafo de Enteros
		grafoIntegers.addVertex(0,0);
		grafoIntegers.addVertex(1,1);
		grafoIntegers.addVertex(2,2);
		grafoIntegers.addVertex(3,3);
		grafoIntegers.addVertex(4,4);
		grafoIntegers.addVertex(5,5);
		grafoIntegers.addVertex(6,6);
		grafoIntegers.addVertex(7,7);
		grafoIntegers.addVertex(8,8);
		grafoIntegers.addVertex(9,9);
		grafoIntegers.addVertex(10,10);
		grafoIntegers.addVertex(11,11);
		grafoIntegers.addVertex(12,12);

		//A�ade los elementos al grafo de Strings
		grafoStrings.addVertex("A","a");
		grafoStrings.addVertex("B","b");
		grafoStrings.addVertex("C","c");
		grafoStrings.addVertex("D","d");
		grafoStrings.addVertex("E","e");
		grafoStrings.addVertex("F","f");
		grafoStrings.addVertex("G","g");
		grafoStrings.addVertex("H","h");
		grafoStrings.addVertex("I","i");	
		grafoStrings.addVertex("J","j");	
		grafoStrings.addVertex("K","k");
		grafoStrings.addVertex("L","l");	
		grafoStrings.addVertex("N","n");
	}
	/**
	 * A�ade todos los arcos al grafo de prueba 
	 */
	private void a�adirArcos()
	{
		//A�ade los arcos del grafo Integer
		grafoIntegers.addEdge(0,1,1);
		grafoIntegers.addEdge(0,2,2);
		grafoIntegers.addEdge(0,6,6);
		grafoIntegers.addEdge(0,5,5);
		grafoIntegers.addEdge(5,3,3);
		grafoIntegers.addEdge(5,4,4);
		grafoIntegers.addEdge(3,4,4);
		grafoIntegers.addEdge(4,6,6);
		grafoIntegers.addEdge(7,8,8);
		grafoIntegers.addEdge(9,10,10);
		grafoIntegers.addEdge(9,11,11);
		grafoIntegers.addEdge(9,12,12);
		grafoIntegers.addEdge(11,12,13);
		//A�ade los arcos del grafo String
		grafoStrings.addEdge("A","B","a");
		grafoStrings.addEdge("A","C","b");
		grafoStrings.addEdge("A","D","c");
		grafoStrings.addEdge("A","E","d");
		grafoStrings.addEdge("B","G","h");
		grafoStrings.addEdge("B","F","g");
		grafoStrings.addEdge("G","F","f");
		grafoStrings.addEdge("F","E","e");
		grafoStrings.addEdge("H","I","i");
		grafoStrings.addEdge("J","K","j");
		grafoStrings.addEdge("J","N","k");
		grafoStrings.addEdge("J","L","l");
		grafoStrings.addEdge("L","N","m");
	}
	/**
	 * Prueba el metodo que retorna la cantidad de vertices de los grafos
	 */
	public void testV()
	{
		setUp();
		assertEquals("El grafo deberia tener 13 nodos",13,grafoIntegers.V());
		assertEquals("El grafo deberia tener 13 nodos",13,grafoStrings.V());
	}

	/**
	 * Prueba el metodo que retorna la cantidad de Arcos de los grafos
	 */
	public void testE()
	{
		setUp();
		assertEquals("El grafo deberia tener 13 arcos",13,grafoIntegers.E());
		assertEquals("El grafo deberia tener 13 arcos",13,grafoStrings.E());
	}
	/**
	 * Prueba el metodo que retorna el valor de un nodo
	 */
	public void testGetInfoVertex()
	{
		setUp();
		assertEquals("El valor retornado por este vertice deberia ser 0",0,(int)grafoIntegers.getInfoVertex(0));
		assertEquals("El valor retornado por este vertice deberia ser 8",8,(int)grafoIntegers.getInfoVertex(8));
		assertEquals("El valor retornado por este vertice deberia ser 12",12,(int)grafoIntegers.getInfoVertex(12));

		assertEquals("El valor retornado por este vertice deberia ser a","a",(String)grafoStrings.getInfoVertex("A"));
		assertEquals("El valor retornado por este vertice deberia ser d","d",(String)grafoStrings.getInfoVertex("D"));
		assertEquals("El valor retornado por este vertice deberia ser g","g",(String)grafoStrings.getInfoVertex("G"));
	}
	/**
	 * Prueba el metodo que retorna la informacion de un arco
	 */
	public void testGetInfoArc()
	{
		setUp();
		assertEquals("La informacion de este arco deberia ser 6", 6,(int)grafoIntegers.getInfoArc(0,6));
		assertEquals("La informacion de este arco deberia ser 6", 5,(int)grafoIntegers.getInfoArc(0,5));
		assertEquals("La informacion de este arco deberia ser 4", 4,(int)grafoIntegers.getInfoArc(3,4));
		assertEquals("La informacion de este arco deberia ser 8", 8,(int)grafoIntegers.getInfoArc(7,8));
		assertEquals("La informacion de este arco deberia ser 12", 12,(int)grafoIntegers.getInfoArc(9,12));

		assertEquals("La informacion de este arco deberia ser a","a",(String)grafoStrings.getInfoArc("A","B"));
		assertEquals("La informacion de este arco deberia ser b","b",(String)grafoStrings.getInfoArc("A","C"));
		assertEquals("La informacion de este arco deberia ser i","i",(String)grafoStrings.getInfoArc("H","I"));
		assertEquals("La informacion de este arco deberia ser k","k",(String)grafoStrings.getInfoArc("J","N"));	
	}
	/**
	 * Prueba el metodo setInfoVertex que cambia el valor de un vertice
	 */
	public void testSetInfoVertex()
	{
		setUp();

		assertEquals("El valor retornado por este vertice deberia ser 0",0,(int)grafoIntegers.getInfoVertex(0));
		grafoIntegers.setInfoVertex(0,7);
		assertEquals("El valor retornado por este vertice deberia ser 7",7,(int)grafoIntegers.getInfoVertex(0));

		assertEquals("El valor retornado por este vertice deberia ser a","a",(String)grafoStrings.getInfoVertex("A"));
		grafoStrings.setInfoVertex("A","z");
		assertEquals("El valor retornado por este vertice deberia ser z","z",(String)grafoStrings.getInfoVertex("A"));
	}
	/**
	 * Prueba el metodo setInfoArco que cambia la informacion de un arco
	 */
	public void testSetInfoArco()
	{
		setUp();

		assertEquals("La informacion de este arco deberia ser 6", 6,(int)grafoIntegers.getInfoArc(0,6));
		grafoIntegers.setInfoArc(0,6,90);
		assertEquals("La informacion de este arco deberia ser 90",90,(int)grafoIntegers.getInfoArc(0,6));

		assertEquals("La informacion de este arco deberia ser b","b",(String)grafoStrings.getInfoArc("A","C"));
		grafoStrings.setInfoArc("A","C","g");
		assertEquals("La informacion de este arco deberia ser g","g",(String)grafoStrings.getInfoArc("A","C"));
	}
	/**
	 * Prueba el metodo adj del grafo
	 */
	public void testAdj()
	{
		setUp();
        //Vertice con varios adjuntos
		int cont1=0;
		IteratorQueue<Integer> it=grafoIntegers.adj(0);
		while (it.hasNext()) 
		{
			it.next();
			cont1++;
		}
		assertEquals("Deberia haber iterado 4 veces", 4, cont1);
        //Vertice con 1 adjunto
		int cont2=0;
		IteratorQueue<Integer> it2=grafoIntegers.adj(8);
		while (it2.hasNext()) 
		{
			it2.next();
			cont2++;
		}
		assertEquals("Deberia haber iterado 1 vez", 1, cont2);
		
		//Vertice con varios adjuntos
		int cont3=0;
		IteratorQueue<String> it3=grafoStrings.adj("A");
		while (it3.hasNext()) 
		{
		    it3.next();
			cont3++;
		}
		assertEquals("Deberia haber iterado 4 veces", 4, cont3);
		//Vertice con 1 adjunto
		int cont4=0;
		IteratorQueue<String> it4=grafoStrings.adj("H");
		while (it4.hasNext()) 
		{
			it4.next();
			cont4++;
		}
		assertEquals("Deberia haber iterado 1 vez", 1, cont4);
	}
	/**
	 * Prueba el metodo addVertex
	 */
	public void testAddVertex()
	{
		setUp();
	    grafoIntegers.addVertex(13,13);
	    assertEquals("El tama�o de los vertices del grafo debio haber aumentado",14,grafoIntegers.V());	
	    grafoStrings.addVertex("M","m");
	    assertEquals("El tama�o de los vertices del grafo debio haber aumentado",14,grafoStrings.V());	
	}
	/**
	 * Prueba el metodo addEdge
	 */
	public void testAddEdge()
	{
		setUp();
	    grafoIntegers.addEdge(7,6,10);
	    assertEquals("El tama�o de los arcos del grafo debio haber aumentado",14,grafoIntegers.E());
	    grafoStrings.addEdge("H","E","z");
	    assertEquals("El tama�o de los arcos del grafo debio haber aumentado",14,grafoStrings.E());
	}
}
