package model.data_structures;

import java.util.Iterator;
import junit.framework.TestCase;


public class TestArbolRojoNegro extends TestCase{

	private ArbolRojoNegro<Integer, String> texto;
	private ArbolRojoNegro<String, Integer> numero;
	
	public void setUp(){
		texto = new ArbolRojoNegro<Integer, String>();
		numero = new ArbolRojoNegro<String, Integer>();
	}
	
	public void setUp1(){
		setUp();
		
		texto.put(1, "a");
		texto.put(2, "b");
		texto.put(3, "c");
		texto.put(4, "d");
		texto.put(5, "e");
		
		numero.put("a", 1);
		numero.put("b", 2);
		numero.put("c", 3);
		numero.put("d", 4);
		numero.put("e", 5);
	}
	
	public void setUp2(){
		setUp();
		
		texto.put(5, "e");
		texto.put(4, "d");
		texto.put(3, "c");
		texto.put(2, "b");
		texto.put(1, "a");
		
		numero.put("e", 5);
		numero.put("d", 4);
		numero.put("c", 3);
		numero.put("b", 2);
		numero.put("a", 1);
	}
	
	public void setUp3(){
		setUp();
		
		texto.put(3, "c");
		texto.put(1, "a");
		texto.put(4, "d");
		texto.put(2, "b");
		texto.put(5, "e");
		
		numero.put("c", 3);
		numero.put("a", 1);
		numero.put("d", 4);
		numero.put("b", 2);
		numero.put("e", 5);
	}
	
	public void testDefinicion() {
		setUp1();
		assertEquals("No cumple con la definicion de una arbol rojo negro", texto.check() ,true );
		assertEquals("No cumple con la definicion de una arbol rojo negro", numero.check() ,true );
		
		setUp2();
		assertEquals("No cumple con la definicion de una arbol rojo negro", texto.check() ,true );
		assertEquals("No cumple con la definicion de una arbol rojo negro", numero.check() ,true );
		
		setUp3();
		assertEquals("No cumple con la definicion de una arbol rojo negro", texto.check() ,true );
		assertEquals("No cumple con la definicion de una arbol rojo negro", numero.check() ,true );
	}
	
	public void testAgregar(){
		setUp();
		texto.put(11, "j");
		numero.put("q", 11);		
		assertEquals("No se agrego correctamente", texto.size(), 1);
		assertEquals("No se agrego correctamente", numero.size(), 1);
		
		texto.put(12, "q");
		assertEquals("Deberia existir esta llave en el arbol", texto.contains(12), true);
		
		numero.put("q", 12);
		assertEquals("Debio hacer cambiado el valor de la llave", numero.get("q") == 12, true);
		
		setUp3();
		assertEquals("No es la llave mayor", texto.max() == 5, true);
		assertEquals("No es la llave mayor", numero.max(), "e");
	}
	
	public void testGet(){
		setUp2();
		texto.put(1, "asd");
		assertEquals("Deberia ser true",texto.get(1).equals("asd"),true);
	}
	
	public void testEliminar(){
		setUp2();
		numero.delete("b");
		assertEquals("Deberia ser falso",numero.contains("b") ,false);
		
		numero.deleteMin();
		assertEquals("Deberia ser falso", numero.contains("a"), false);
		
		numero.deleteMax();
		assertEquals("Deberia ser falso", numero.contains("e"), false);
		
		assertEquals("El tama�o debio haber cambiado", numero.size(), 2);
	}
	
	public void testKeys(){
		setUp1();
		
		Iterator<Integer> x = texto.keys().iterator();
		int i = 1;
		while(x.hasNext()){
			if(x.next() != i) assertTrue("", true);
			i++;
		}
		
	}
	
	public void testIsEmpty(){
		setUp();
		assertEquals("Deberia estar vacio",numero.isEmpty(),true);
		numero.put("q", 11);		
		assertEquals("Ya hay un elemento", texto.size() == 1, false);
	}
	
}
