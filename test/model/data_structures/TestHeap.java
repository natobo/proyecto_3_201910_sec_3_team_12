package model.data_structures;

import junit.framework.TestCase;
import model.data_structures.ArregloDinamico;
import model.data_structures.MaxHeapCP;


public class TestHeap extends TestCase
{

	/**
	 * Heap a probar con Strings
	 */
	private MaxHeapCP<String> heapString;

	/**
	 * Heap a probar con Integers
	 */
	private MaxHeapCP<Integer> heapInteger;


	/**
	 * Crea el escenario de prueba.
	 */
	public void setUp() 
	{
		try
		{
			//Crea el heap de enteros
			heapInteger=new MaxHeapCP<Integer>(10);

			//A�ade elementos al heap de enteros.

			heapInteger.agregar(3);
			heapInteger.agregar(2);
			heapInteger.agregar(1);
			heapInteger.agregar(10);
			heapInteger.agregar(9);
			heapInteger.agregar(7);
			heapInteger.agregar(8);
			heapInteger.agregar(4);
			heapInteger.agregar(6);
			heapInteger.agregar(5);

			//Crea la cola de strings
			heapString=new MaxHeapCP<String>(10);
			//A�ade elementos al heap de enteros.
			heapString.agregar("b");
			heapString.agregar("c");
			heapString.agregar("a");
			heapString.agregar("e");
			heapString.agregar("d");
			heapString.agregar("f");
			heapString.agregar("h");
			heapString.agregar("i");
			heapString.agregar("j");
			heapString.agregar("g");


		}
		catch (Exception e) 
		{
			// TODO: handle exception
			fail("Los heaps no se han podido inicializar");
		}

	}

	/**
	 * Prueba si agrega un elemento dentro de un Heap
	 */
	public void testAgregar()
	{
		setUp();
 
		assertTrue("Deberia haber aumentado el numero de elementos",heapInteger.darNumElementos()==10);
		assertTrue("Deberia haber aumentado el numero de elementos",heapString.darNumElementos()==10);
		
	}
	
	/**
	 * Prueba si agrega un elemento dentro de un Heap
	 */
	public void testDelMax()
	{
		setUp();

		Integer rtaInt=heapInteger.delMax();
		assertTrue("Deberia haber retornado el mayor elemento",rtaInt==10);
		assertTrue("Deberia haber dismuido el numero de elementos",heapInteger.darNumElementos()==9);
		
		Integer rtaInt2=heapInteger.delMax();
		assertTrue("Deberia haber retornado el mayor elemento",rtaInt2==9);
		assertTrue("Deberia haber dismuido el numero de elementos",heapInteger.darNumElementos()==8);
		
		Integer rtaInt3=heapInteger.delMax();
		assertTrue("Deberia haber retornado el mayor elemento",rtaInt3==8);
		assertTrue("Deberia haber dismuido el numero de elementos",heapInteger.darNumElementos()==7);
	
		
		String rtaStr=heapString.delMax();
		assertTrue("Deberia haber retornado el mayor elemento",rtaStr.equals("j"));
		assertTrue("Deberia haber dismuido el numero de elementos",heapString.darNumElementos()==9);
		
		String rtaStr2=heapString.delMax();
		assertTrue("Deberia haber retornado el mayor elemento",rtaStr2.equals("i"));
		assertTrue("Deberia haber dismuido el numero de elementos",heapString.darNumElementos()==8);
		
		String rtaStr3=heapString.delMax();
		assertTrue("Deberia haber retornado el mayor elemento",rtaStr3.equals("h"));
		assertTrue("Deberia haber dismuido el numero de elementos",heapString.darNumElementos()==7);
		
		
	}
}
