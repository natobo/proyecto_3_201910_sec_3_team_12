package model.data_structures;

import static org.junit.Assert.fail;
import java.util.Iterator;
import junit.framework.TestCase;

public class TestQueue extends TestCase
{	

	/**
	 * Es la cola de strings donde se har�n las pruebas
	 */
	private Queue<String>  colaStrings;
	/**
	 * Es la cola de Integers donde se har�n las pruebas
	 */
	private Queue<Integer> colaIntegers;

	public void setUp() 
	{
		try
		{
			//Crea la cola de enteros
			colaIntegers=new Queue<Integer>();
			//A�ade elementos a la cola de enteros.
			colaIntegers.enqueue(1);
			colaIntegers.enqueue(2);
			colaIntegers.enqueue(3);
			colaIntegers.enqueue(4);
			colaIntegers.enqueue(5);
			colaIntegers.enqueue(6);

			//Crea la cola de strings
			colaStrings=new Queue<String>();
			//A�ade elementos a la cola de strings.
			colaStrings.enqueue("a");
			colaStrings.enqueue("b");
			colaStrings.enqueue("c");
			colaStrings.enqueue("d");
			colaStrings.enqueue("e");
			colaStrings.enqueue("f");
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			fail("Las colas no se han podido inicializar");
		}

	}

	/**
	 * Comprueba si retorna el iterador de objetos de las colas y si retorna el objeto con su tipo correspondiente
	 */
	public void testIterator() 
	{
		setUp();

		Iterator<Integer> itInteger =colaIntegers.iterator();
		Iterator<String> itStrings= colaStrings.iterator();

		assertNotNull(itInteger);
		assertNotNull(itStrings);

		int x=itInteger.next();

		assertEquals("Deberia ser un 1 el objeto retornado ",1,x);

		String y=itStrings.next();

		assertEquals("Deberia ser una a el objeto retornado ","a",y);
	}

	/**
	 * Comprueba si retorna correctamente el tama�o de cada cola.
	 */
	public void testSize()
	{
		setUp();

		assertTrue("No esta retornando el tama�o adecuado",colaIntegers.size()==6);
		assertTrue("No esta retornando el tama�o adecuado",colaStrings.size()==6);

		//Elimina un elemento para comprobar si cambia el tama�o
		colaStrings.dequeue();
		colaIntegers.dequeue();

		assertTrue("No esta retornando el tama�o adecuado",colaIntegers.size()==5);
		assertTrue("No esta retornando el tama�o adecuado",colaStrings.size()==5);
	}

	/**
	 * Comprueba si retorna correctamente el booleano.
	 */
	public void testIsEmpty()
	{
		setUp();

		assertFalse("Deberia ser falso", colaIntegers.isEmpty());
		assertFalse("Deberia ser falso", colaStrings.isEmpty());

		colaIntegers.dequeue();
		colaIntegers.dequeue();
		colaIntegers.dequeue();
		colaIntegers.dequeue();
		colaIntegers.dequeue();
		colaIntegers.dequeue();

		colaStrings.dequeue();
		colaStrings.dequeue();
		colaStrings.dequeue();
		colaStrings.dequeue();
		colaStrings.dequeue();
		colaStrings.dequeue();

		assertTrue("Deberia ser verdadero ",colaIntegers.isEmpty());
		assertTrue("Deberia ser verdadero",colaStrings.isEmpty());
	}
	/**
	 * Comprueba que si agrega un elemento 
	 */
	public void testEnqueue()
	{
		setUp();

		colaIntegers.enqueue(9);

		Iterator<Integer> itInt=colaIntegers.iterator();
		boolean loEncontro=false;
		while(itInt.hasNext())
		{
			int x=itInt.next();
			if(x==9)
				loEncontro=true;
		}
		assertTrue("Deberia haber encontrado el elemento",loEncontro);

		colaStrings.enqueue("z");

		Iterator<String> itStr=colaStrings.iterator();
		boolean loEncontro2=false;
		while(itStr.hasNext())
		{
			String y=itStr.next();
			if(y.equalsIgnoreCase("z"))
				loEncontro2=true;
		}
		assertTrue("Deberia haber encontrado el elemento",loEncontro2);

	}

	/**
	 * Comprueba si retorna el primer elemento y si lo elimina de la cola
	 */
	public void testDequeue()
	{
		setUp();

		int x=colaIntegers.dequeue();

		assertEquals("Deberia haber retornado un uno",1,x);

		Iterator<Integer> itInt=colaIntegers.iterator();
		boolean loEncontro=false;
		while(itInt.hasNext())
		{
			int y=itInt.next();
			if(y==9)
				loEncontro=true;
		}

		assertFalse("No deberia haberlo encontrado ",loEncontro);

		String z=colaStrings.dequeue();

		assertEquals("Deberia haber retornado una a","a",z);

		Iterator<String> itStr=colaStrings.iterator();
		boolean loEncontro2=false;
		while(itStr.hasNext())
		{
			String y=itStr.next();
			if(y.equalsIgnoreCase("a"))
				loEncontro2=true;
		}

		assertFalse("No deberia haberlo encontrado ",loEncontro2);

	}
}
