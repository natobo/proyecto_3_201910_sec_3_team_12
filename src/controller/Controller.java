package controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;

import javax.swing.text.TabableView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opencsv.CSVReader;
import com.teamdev.jxmaps.LatLng;
import GeneradorMapa.Mapa;
import jdk.nashorn.internal.ir.WhileNode;
import jdk.nashorn.internal.parser.JSONParser;
import model.data_structures.Kruskal;
import model.data_structures.ArbolRojoNegro;
import model.data_structures.Arco;
import model.data_structures.ArregloDinamico;
import model.data_structures.BFS;
import model.data_structures.ColaPrioridad;
import model.data_structures.CC;
import model.data_structures.DijkstraPorDistancia;
import model.data_structures.DijkstraPorInfracciones;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.HashSeparateChaining;
import model.data_structures.Queue;
import model.data_structures.Vertice;
import model.data_structures.IteratorQueue;
import model.data_structures.MaxHeapCP;
import model.data_structures.MinPQ;
import model.data_structures.Prim;
import model.vo.CordVO;
import model.vo.EstadisticasCargaInfracciones;
import model.vo.VOMovingViolations;
import model.vo.pesoWay;
import view.MovingViolationsManagerView;

public class Controller 
{
	//
	// CONSTANTES
	//
	/**
	 * Radio terrestre medio, en kilometros;
	 */
	private static Double RadioTierra = (double) 6371000; 
	/**
	 * Constante para transformar grados en radianes
	 */
	private static Double constanteRad= (double) ((Math.PI)/180); 
	/**
	 * Grafo a cargar con los datos de las vias.
	 */
	private GrafoNoDirigido<String, CordVO, pesoWay> grafoVias;
	/**
	 * Lista de meses del programa
	 */
	private String[] listaMes=new String[12];
	/**
	 * numero total de infracciones
	 */
	private int numTotalInfrac;
	/**
	 * Tabla que contiene todas las infracciones del a�o 2018
	 **/
	private HashSeparateChaining<String, VOMovingViolations>  tablaInfracciones;
	/**
	 * Estructura para resolver requerimento 1A
	 */
	private DijkstraPorInfracciones djPorInfacciones;
	/**
	 * Estructura para resolver requerimento 3C
	 */
	private DijkstraPorDistancia djPorDistancia;
	/**
	 * Clase que carga una consulta Bfs en el controller
	 */
	private BFS consultaBFS;
	/**
	 * Arreglo para guardar los vertices del Req 2B
	 */
	private ArregloDinamico<String> verticesMatriz;
	// Componente vista (consola)
	private MovingViolationsManagerView view;

	private GrafoNoDirigido<String, CordVO, pesoWay>graf = new GrafoNoDirigido<String, CordVO, pesoWay>();
	//TODO Definir los atributos de estructuras de datos del modelo del mundo del proyecto


	/**
	 * Metodo constructor
	 */
	public Controller()
	{
		view = new MovingViolationsManagerView();
		grafoVias=new GrafoNoDirigido<String, CordVO, pesoWay>();

		tablaInfracciones=new HashSeparateChaining<String, VOMovingViolations>(10000000);

		listaMes[0]="January";
		listaMes[1]="February";
		listaMes[2]="March";
		listaMes[3]="Abril";
		listaMes[4]="May";
		listaMes[5]="June";
		listaMes[6]="July";
		listaMes[7]="August";
		listaMes[8]="September";
		listaMes[9]="October";
		listaMes[10]="November";
		listaMes[11]="December";
	}

	/**
	 * Metodo encargado de ejecutar los  requerimientos segun la opcion indicada por el usuario
	 */
	public void run(){

		long startTime;
		long endTime;
		long duration;

		Scanner sc = new Scanner(System.in);
		boolean fin = false;


		while(!fin){
			view.printMenu();

			int option = sc.nextInt();
			int idVertice1 = 0;
			int idVertice2 = 0;


			switch(option){

			case 0:
				String RutaArchivo = "";
				view.printMessage("Escoger el grafo a cargar: (1) Downtown  o (2)Ciudad Completa.");
				int ruta = sc.nextInt();
				if(ruta == 1)
					RutaArchivo = ""; //TODO Dar la ruta del archivo de Downtown
				else
					RutaArchivo = "./data/finalGraph.json"; //TODO Dar la ruta del archivo de la ciudad completa

				startTime = System.currentTimeMillis();
				//Carga el Json
				loadJSON(RutaArchivo);
				//Crea tablas "interpretes"
				grafoVias.crearTablas();
				//Guarda las infracciones en una tabla hash
				loadMovingViolations();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				// TODO Informar el total de v�rtices y el total de arcos que definen el grafo cargado
				LatLng max = new LatLng(38.9852065,-76.2653718);
				LatLng min = new LatLng(38.766235,-78.284922);
//				generarMapaGrafo(grafoVias, min, max, false,null);
				break;

			case 1:

				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				idVertice1 = sc.nextInt();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				idVertice2 = sc.nextInt();
				startTime = System.currentTimeMillis();
				System.out.println("Por favor esperar 2 min...");
				caminoCostoMinimoA1(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/* 
				TODO Consola: Mostrar el camino a seguir con sus v�rtices (Id, Ubicaci�n Geogr�fica),
				el costo m�nimo (menor cantidad de infracciones), y la distancia estimada (en Km).
				 */
				GrafoNoDirigido<String, CordVO, pesoWay> x = new GrafoNoDirigido<String, CordVO, pesoWay>();
				System.out.println("Desde: "+idVertice1+" Hasta: "+idVertice2);
				HashSeparateChaining<String,Integer> iA=grafoVias.getTablaI();
				HashSeparateChaining<Integer,String> irA=grafoVias.getTablaRI();
				Iterable<Arco<String, CordVO, pesoWay>> pilaCamino2=djPorInfacciones.pathTo(iA.get(idVertice2+""));
				Iterator<Arco<String, CordVO, pesoWay>> it2= pilaCamino2.iterator();	

				Double distPromedio=0.0;
				while(it2.hasNext())
				{
					Arco<String, CordVO, pesoWay> arco=it2.next();
					x.addVertex(arco.getInicial().getId(),arco.getInicial().getInfo());
					x.addVertex(arco.getFinal().getId(),arco.getFinal().getInfo());
					x.addEdge(arco.getInicial().getId(), arco.getFinal().getId(), arco.getInfoArco());
					CordVO vInicial=arco.getInicial().getInfo();
					CordVO vFinal=arco.getFinal().getInfo();
					System.out.println("Vertice inicial:"+arco.getInicial().getId()+ "Lat:"+vInicial.getLat()+" Lon:"+ vInicial.getLong());
					System.out.println("Vertice Final: "+arco.getFinal().getId()+" Lat:"+vFinal.getLat()+" Lon:"+ vFinal.getLong());
					distPromedio+=arco.getInfoArco().getDistHarvesiana();
				}

				System.out.println("Distancia promedio km: "+distPromedio);

				/*
				TODO Google Maps: Mostrar el camino resultante en Google Maps 
				(incluyendo la ubicaci�n de inicio y la ubicaci�n de destino).
				 */
				LatLng max1 = new LatLng(38.9852065,-76.2653718);
				LatLng min1 = new LatLng(38.766235,-78.284922);
				generarMapaGrafo(x, min1, max1, false,null);

				break;

			case 2:
				view.printMessage("2A. Consultar los N vertices con mayor numero de infracciones. Ingrese el valor de N: ");
				int n = sc.nextInt();


				startTime = System.currentTimeMillis();
				GrafoNoDirigido<String, CordVO, pesoWay>graft = mayorNumeroVerticesA2(n);
				CC cc = new CC(graft);


				int numGraf = cc.count();


				HashSeparateChaining<String, Integer> ids = cc.getId();
				HashSeparateChaining<Integer, Integer> size = cc.getSize();
				Iterator<Integer> iter = size.keys();
				MaxHeapCP<Integer> num = new MaxHeapCP<Integer>(numGraf+1);
				while(iter.hasNext()) {
					int idComp = iter.next();
					num.agregar(cc.size(idComp));
				}

				Iterator<String> iter2 = ids.keys();
				int mayor = num.max();
				GrafoNoDirigido<String, CordVO, pesoWay> g = new GrafoNoDirigido<String, CordVO, pesoWay>();
				ArregloDinamico<Vertice<String, CordVO, pesoWay>> vert = new ArregloDinamico<Vertice<String, CordVO, pesoWay>>(mayor+1);
				while(iter2.hasNext()) {
					String idV= iter2.next();
					if(cc.size(cc.id(idV)) == mayor) {
						Vertice<String, CordVO, pesoWay> ver = graft.darVertice(idV);
						g.addVertex(idV+"", ver.getInfo());
						mayor = cc.id(idV);
						vert.agregar(ver);
					}
				}



				for(int i = 0; i < vert.darTamano(); i++) {
					Vertice<String, CordVO, pesoWay> vertex = vert.darElemento(i);
					Queue<Arco<String, CordVO, pesoWay>> arcos = vertex.getAdyacentes();
					while(arcos.darPrimero() != null) {
						Arco<String, CordVO, pesoWay> arco = arcos.dequeue();
						for(int j = i+1; j < n; j++) {
							String s = vert.darElemento(j).getId();
							if(arco.getFinal().getId().equals(s)) {
								g.addEdge(arco.getInicial().getId(), arco.getFinal().getId(), arco.getInfoArco());
							}
						}
					}
				}

				graf = g;

				endTime = System.currentTimeMillis();

				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/* 
				TODO Consola: Mostrar la informacion de los n vertices 
				(su identificador, su ubicaci�n (latitud, longitud), y el total de infracciones) 
				Mostra el n�mero de componentes conectadas (subgrafos) y los  identificadores de sus vertices 
				 */

				ArregloDinamico<String> keys1 =graft.keys();
				for(int i = 0; i < keys1.darTamano(); i++) {
					Vertice<String, CordVO, pesoWay> vert1 = graft.darVertice(keys1.darElemento(i));
					view.printMessage("Id: "+vert1.getId()+"; "+ vert1.getInfo().toString()+"; "+vert1.darNumInf());
				}

				view.printMessage("El n�mero de componentes conectadas es: "+numGraf);

				/*
				TODO Google Maps: Marcar la localizaci�n de los v�rtices resultantes en un mapa en
				Google Maps usando un color 1. Destacar la componente conectada m�s grande (con
				m�s v�rtices) usando un color 2. 
				 */
				LatLng max11 = new LatLng(38.9852065,-76.2653718);
				LatLng min11 = new LatLng(38.766235,-78.284922);
				generarMapaGrafo(graft, min11, max11,false, g);
				break;

			case 3:			

				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				idVertice1 = sc.nextInt();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				idVertice2 = sc.nextInt();


				startTime = System.currentTimeMillis();
				System.out.println("Por favor esperar 2 min...");
				caminoLongitudMinimoaB1(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar  el camino a seguir, informando
					el total de v�rtices, sus v�tices (Id, Ubicaci�n Geogr�fica) y la distancia estimada (en Km).
				 */
				//Crea un grafo para representar el requerimento en google Maps
				GrafoNoDirigido<String, CordVO, pesoWay>grafoRepreMapa=new GrafoNoDirigido<String, CordVO, pesoWay>();


				System.out.println("-----------------CAMINO BFS:--------------");
				System.out.println("Desde: "+idVertice1+" Hasta: "+idVertice2);
				HashSeparateChaining<String,Integer> i=grafoVias.getTablaI();
				HashSeparateChaining<Integer,String> ir=grafoVias.getTablaRI();
				Iterable<Integer> pilaCamino= (Iterable<Integer>) consultaBFS.pathTo(i.get(idVertice2+""));
				Iterator<Integer> it= pilaCamino.iterator();
				int contVertices=0;
				Double distMedia=0.0;
				String tmpFinal=idVertice2+""; 
				String tmpInicial="";		
				while(it.hasNext())
				{
					String idVertice=ir.get(it.next());
					tmpInicial=idVertice;
					CordVO cord=grafoVias.getInfoVertex(idVertice);
					System.out.println("Vertice: "+idVertice +" Coordenadas(Lat,Long): "+cord.getLat()+","+cord.getLong());
					if(!tmpFinal.equals(tmpInicial))
					{
						grafoRepreMapa.addVertex(tmpInicial, grafoVias.getInfoVertex(tmpInicial));
						grafoRepreMapa.addVertex(tmpFinal, grafoVias.getInfoVertex(tmpFinal));
						pesoWay peso=grafoVias.getInfoArc(tmpFinal,tmpInicial);
						grafoRepreMapa.addEdge(tmpFinal, tmpInicial, peso);
						distMedia+=peso.getDistHarvesiana();
					}
					tmpFinal=idVertice;
					contVertices++;
				}
				System.out.println("Total de vertices: "+contVertices);
				System.out.println("Distancia promedio: "+distMedia +" km" );

				/*
				   TODO Google Maps: Mostre el camino resultante en Google Maps (incluyendo la
					ubicaci�n de inicio y la ubicaci�n de destino).
				 */
				//Coordenada minima Area
				//				LatLng min = new LatLng(38.8888,-76.9589);
				//				//Coordenada maxima Area
				//				LatLng max = new LatLng(38.9226,-77.0733);
				LatLng max2 = new LatLng(38.9852065,-76.2653718);
				LatLng min2 = new LatLng(38.766235,-78.284922);
				generarMapaGrafo(grafoRepreMapa, min2, max2,false, null);

				break;

			case 4:		
				double lonMin;
				double lonMax;
				view.printMessage("Ingrese la longitud minima (Ej. -87,806): ");
				lonMin = sc.nextDouble();
				view.printMessage("Ingrese la longitud maxima (Ej. -87,806): ");
				lonMax = sc.nextDouble();

				view.printMessage("Ingrese la latitud minima (Ej. 44,806): ");
				double latMin = sc.nextDouble();
				view.printMessage("Ingrese la latitud maxima (Ej. 44,806): ");
				double latMax = sc.nextDouble();

				view.printMessage("Ingrese el numero de columnas");
				int columnas = sc.nextInt();
				view.printMessage("Ingrese el numero de filas");
				int filas = sc.nextInt();


				startTime = System.currentTimeMillis();
				verticesMatriz=definirCuadriculaB2(lonMin,lonMax,latMin,latMax,columnas,filas);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar el n�mero de v�rtices en el grafo
					resultado de la aproximaci�n. Mostar el identificador y la ubicaci�n geogr�fica de cada
					uno de estos v�rtices. 
				 */	
				//Grafo para repesentar Requerimento
				GrafoNoDirigido<String, CordVO, pesoWay>grafoRepreMapa4=new GrafoNoDirigido<String, CordVO, pesoWay>();

				int contador=0;

				for (int j = 0; j < verticesMatriz.darTamano(); j++) 
				{
					String idVertice=verticesMatriz.darElemento(j);
					CordVO cord=grafoVias.getInfoVertex(idVertice);
					System.out.println("Vertice: "+idVertice +" Coordenadas(Lat,Long): "+cord.getLat()+","+cord.getLong());
					grafoRepreMapa4.addVertex(idVertice, cord);
					contador++;
				}


				/*
				   TODO Google Maps: Marcar las ubicaciones de los v�rtices resultantes de la
					aproximaci�n de la cuadr�cula en Google Maps.
				 */
				LatLng max4 = new LatLng(38.9852065,-76.2653718);
				LatLng min4 = new LatLng(38.766235,-78.284922);

				generarMapaGrafo(grafoRepreMapa4, min4, max4,true,null);
				break;

			case 5:

				startTime = System.currentTimeMillis();
				System.out.println(graf.E());
				GrafoNoDirigido<String, CordVO, pesoWay> krus = arbolMSTKruskalC1(graf);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar los v�rtices (identificadores), los arcos incluidos (Id v�rtice inicial e Id v�rtice
					final), y el costo total (distancia en Km) del �rbol.
				 */
				view.printMessage("Los vertices son:");
				ArregloDinamico<String> keyss = krus.keys();
				for(int q = 0;q<keyss.darTamano(); q++) {
					view.printMessage(keyss.darElemento(q));
				}
				view.printMessage("Los arcos son:");
				Queue<Arco<String, CordVO, pesoWay>> arcos = krus.darArcos();
				for(int q=0;q<arcos.darTamano();q++) {
					Arco<String, CordVO, pesoWay> arc = arcos.dequeue();
					view.printMessage("Id v�rtice inicial: "+ arc.getInicial().getId()+"Id v�rtice final: "+arc.getFinal().getId());
				}

				double s = darDist(arcos);
				view.printMessage("La distancia (costo) es: "+s);
				/*
				   TODO Google Maps: Mostrar el �rbol generado resultante en Google Maps: sus v�rtices y sus arcos.
				 */
				LatLng max5 = new LatLng(38.9852065,-76.2653718);
				LatLng min5 = new LatLng(38.766235,-78.284922);

				generarMapaGrafo(krus, min5, max5,false,null);

				break;

			case 6:

				startTime = System.currentTimeMillis();
				GrafoNoDirigido<String, CordVO, pesoWay> gb = arbolMSTPrimC2();
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar los v�rtices (identificadores), los arcos incluidos (Id v�rtice inicial e Id v�rtice
				 	final), y el costo total (distancia en Km) del �rbol.
				*/
				view.printMessage("Los vertices son:");
				ArregloDinamico<String> keyss1 = gb.keys();
				for(int q = 0;q<keyss1.darTamano(); q++) {
					view.printMessage(keyss1.darElemento(q));
				}
				view.printMessage("Los arcos son:");
				Queue<Arco<String, CordVO, pesoWay>> arcos1 = gb.darArcos();
				for(int q=0;q<arcos1.darTamano();q++) {
					Arco<String, CordVO, pesoWay> arc = arcos1.dequeue();
					view.printMessage("Id v�rtice inicial: "+ arc.getInicial().getId()+"Id v�rtice final: "+arc.getFinal().getId());
				}
				/*
				   TODO Google Maps: Mostrar el �rbol generado resultante en Google Maps: sus v�rtices y sus arcos.
				 */
				LatLng max51 = new LatLng(38.9852065,-76.2653718);
				LatLng min51 = new LatLng(38.766235,-78.284922);

				generarMapaGrafo(gb, min51, max51,false,null);
				break;

			case 7:

				startTime = System.currentTimeMillis();
				caminoCostoMinimoDijkstraC3(verticesMatriz);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar de cada camino resultante: su secuencia de v�rtices (identificadores) y su costo (distancia en Km).

				   TODO Google Maps: Mostrar los caminos de costo m�nimo en Google Maps: sus v�rtices
					y sus arcos. Destaque el camino m�s largo (en distancia) usando un color diferente
				 */
				break;

			case 8:
				view.printMessage("Ingrese El id del primer vertice (Ej. 901839): ");
				idVertice1 = sc.nextInt();
				view.printMessage("Ingrese El id del segundo vertice (Ej. 901839): ");
				idVertice2 = sc.nextInt();

				startTime = System.currentTimeMillis();
				caminoMasCortoC4(idVertice1, idVertice2);
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				view.printMessage("Tiempo del requerimiento: " + duration + " milisegundos");
				/*
				   TODO Consola: Mostrar del camino resultante: su secuencia de v�rtices (identificadores), 
				   el total de infracciones y la distancia calculada (en Km).
				 */
				GrafoNoDirigido<String, CordVO, pesoWay>grafoRepreMapa2=new GrafoNoDirigido<String, CordVO, pesoWay>();

				System.out.println("-----------------CAMINO BFS:--------------");
				System.out.println("Desde: "+idVertice1+" Hasta: "+idVertice2);
				HashSeparateChaining<String,Integer> i2=grafoVias.getTablaI();
				HashSeparateChaining<Integer,String> ir2=grafoVias.getTablaRI();
				Iterable<Integer> pilaCamino21= (Iterable<Integer>) consultaBFS.pathTo(i2.get(idVertice2+""));
				Iterator<Integer> it21= pilaCamino21.iterator();
				int contVertices2=0;
				Double distMedia2=0.0;
				String tmpFinal2=idVertice2+""; 
				String tmpInicial2="";		
				while(it21.hasNext())
				{
					String idVertice21=ir2.get(it21.next());
					tmpInicial2=idVertice21;
					CordVO cord=grafoVias.getInfoVertex(idVertice21);
					System.out.println("Vertice: "+idVertice21 +" Coordenadas(Lat,Long): "+cord.getLat()+","+cord.getLong());
					if(!tmpFinal2.equals(tmpInicial2))
					{
						grafoRepreMapa2.addVertex(tmpInicial2, grafoVias.getInfoVertex(tmpInicial2));
						grafoRepreMapa2.addVertex(tmpFinal2, grafoVias.getInfoVertex(tmpFinal2));
						pesoWay peso=grafoVias.getInfoArc(tmpFinal2,tmpInicial2);
						grafoRepreMapa2.addEdge(tmpFinal2, tmpInicial2, peso);
						distMedia2+=peso.getDistHarvesiana();
					}
					tmpFinal=idVertice21;
					contVertices2++;
				}
				System.out.println("Total de vertices: "+contVertices2);
				System.out.println("Distancia promedio: "+distMedia2 +" km" );
				/*
				   TODO Google Maps: Mostrar  el camino resultante en Google Maps: sus v�rtices y sus arcos.
				 *
				 */
				LatLng max21 = new LatLng(38.9852065,-76.2653718);
				LatLng min21 = new LatLng(38.766235,-78.284922);

				generarMapaGrafo(grafoRepreMapa2, min21, max21,false,null);
				break;

			case 9: 	
				fin = true;
				sc.close();
				break;
			}
		}
	}


	// TODO El tipo de retorno de los metodos puede ajustarse segun la conveniencia


	// TODO El tipo de retorno de los metodos puede ajustarse segun la conveniencia


	/**
	 * Cargar el Grafo No Dirigido de la malla vial: Downtown o Ciudad Completa
	 * @param rutaArchivo 
	 */
	public void loadJSON(String rutaArchivo) 
	{
		// TODO Auto-generated method stub
		GrafoNoDirigido<String, CordVO, pesoWay> copy = grafoVias;
		grafoVias= new GrafoNoDirigido<String, CordVO, pesoWay>();
		CordVO min=null;
		CordVO max=null;
		try
		{
			JsonParser parser = new JsonParser();
			BufferedReader bufferedReader = new BufferedReader(new FileReader(rutaArchivo));
			JsonArray gsonArr = parser.parse(bufferedReader).getAsJsonArray();
			//Carga todos los vertices
			for (JsonElement obj : gsonArr) 
			{
				// Object of array
				JsonObject gsonObj = obj.getAsJsonObject();
				// Primitives elements of object
				String id = gsonObj.get("id").getAsString();
				Double lat = gsonObj.get("lat").getAsDouble();
				Double lon = gsonObj.get("lon").getAsDouble();
				// List of primitive elements
				JsonArray idAdj = gsonObj.get("adj").getAsJsonArray();	

				Queue<String> colaVids=new Queue<String>();

				for (JsonElement idTmp : idAdj) 
				{
					colaVids.enqueue(idTmp.getAsString());
				}

				JsonArray infracciones = gsonObj.get("infractions").getAsJsonArray();
				Queue<String> infrac=new Queue<String>();
				for (JsonElement idTmp : infracciones) 
				{
					infrac.enqueue(idTmp.getAsString());
				}
				CordVO cord=new CordVO(lat, lon);
				if(min==null)
				{	
					min=cord;
				}
				if(max==null)
				{	
					max=cord;
				}
				if(max!=null&&cord.getLat()>min.getLat()&&cord.getLong()>min.getLong())
				{	
					max=cord;
				}
				if(min!=null&&cord.getLat()<min.getLat()&&cord.getLong()<min.getLong())
				{	
					min=cord;
				}
				grafoVias.addVertexWithQueue(id, cord, infrac, colaVids);
			}

			//Carga todos los arcos

			ArregloDinamico<String> llavesGrafo=grafoVias.keys();

			for (int i = 0; i < llavesGrafo.darTamano(); i++) 
			{
				try
				{
					Vertice<String, CordVO, pesoWay> v=grafoVias.darVertice(llavesGrafo.darElemento(i));
					Queue<String> idAdj=v.getIdVAdj();
					IteratorQueue<String> iteratorAdj=idAdj.iterator(); //COLA DE ADYACENTES TIENE VERTICES QUE NO EXISTEN 
					while (iteratorAdj.hasNext()) 
					{
						String string = (String) iteratorAdj.next();
						CordVO tmp=grafoVias.getInfoVertex(string);
						pesoWay peso=new pesoWay("",calcularDistanciaHarversiana(v.getInfo(), tmp));
						grafoVias.addEdge(v.getId(),string,peso);
					}
				} 
				catch (Exception e) 
				{
					// TODO: handle exception
				}
			}

			System.out.println("Informacion del grafo cargado: "+grafoVias.toString());

			System.out.println("Coordenada maxima(Lat,Long): "+ max.getLat()+","+max.getLong());
			System.out.println("Coordenada minima(Lat,Long: "+min.getLat()+","+min.getLong());

			//Carga todas las infracciones
			//			loadMovingViolations();
			//Carga las tablas Integer paralelas del grafo
			grafoVias.crearTablas();
			//			System.out.println("Total de infracciones cargadas :: "+numTotalInfrac);

		}
		catch (Exception e) 
		{
			e.printStackTrace();
			grafoVias = copy;
		}
	}




	// TODO El tipo de retorno de los metodos puede ajustarse segun la conveniencia
	/**
	 * Requerimiento 1A: Encontrar el camino de costo minimo para un viaje entre dos ubicaciones geograficas.
	 * @param idVertice2 
	 * @param idVertice1 
	 */
	public void caminoCostoMinimoA1(int idVertice1, int idVertice2)
	{
		HashSeparateChaining<String,Integer> interprete=grafoVias.getTablaI();
		Queue<Arco<String, CordVO, pesoWay>>[] param=darAdyacentesNumericos();
		djPorInfacciones=new DijkstraPorInfracciones(grafoVias, interprete.get(idVertice1+""), param);

	}

	// TODO El tipo de retorno de los metodos puede ajustarse segun la conveniencia
	/**
	 * Requerimiento 2A: Determinar los n vertices con mayor numero de infracciones. Adicionalmente identificar las
	 * componentes conectadas (subgrafos) que se definan unicamente entre estos n vertices
	 * @param  int n: numero de vertices con mayor numero de infracciones  
	 */
	public GrafoNoDirigido<String, CordVO, pesoWay> mayorNumeroVerticesA2(int n) {

		// TODO Auto-generated method stub
		GrafoNoDirigido<String, CordVO, pesoWay> graf = new GrafoNoDirigido<String, CordVO, pesoWay>();
		MaxHeapCP<Vertice<String, CordVO, pesoWay>> max = new MaxHeapCP<Vertice<String, CordVO, pesoWay>>(grafoVias.V()+1);
		ArregloDinamico<String> keys1 = grafoVias.keys();
		for(int i = 0; i < keys1.darTamano(); i++) {
			max.agregar(grafoVias.darVertice(keys1.darElemento(i)));
		}
		ArregloDinamico<Vertice<String, CordVO, pesoWay>> vert = new ArregloDinamico<Vertice<String, CordVO, pesoWay>>(n);
		for(int i = 0; i < n;i++) {
			Vertice<String, CordVO, pesoWay> vertex = max.delMax();
			graf.addVertex(vertex.getId(), vertex.getInfo());
			vert.agregar(vertex);;
		}
		for(int i = 0; i < n; i++) {
			Vertice<String, CordVO, pesoWay> vertex = vert.darElemento(i);
			Queue<Arco<String, CordVO, pesoWay>> arcos = vertex.getAdyacentes();
			while(arcos.darPrimero() != null) {
				Arco<String, CordVO, pesoWay> arco = arcos.dequeue();
				for(int j = i+1; j < n; j++) {
					String s = vert.darElemento(j).getId();
					if(arco.getFinal().getId().equals(s)) {
						graf.addEdge(arco.getInicial().getId(), arco.getFinal().getId(), arco.getInfoArco());
					}
				}
			}
		}

		return graf;
	}

	// TODO El tipo de retorno de los metodos puede ajustarse segun la conveniencia
	/**
	 * Requerimiento 1B: Encontrar el camino mas corto para un viaje entre dos ubicaciones geograficas 
	 * @param idVertice2 
	 * @param idVertice1 
	 */
	public void caminoLongitudMinimoaB1(int idVertice1, int idVertice2) 
	{
		// TODO Auto-generated method stub
		HashSeparateChaining<String,Integer> interprete=grafoVias.getTablaI();
		consultaBFS=new BFS<String, CordVO, pesoWay>(grafoVias,interprete.get(idVertice1+""));
	}

	// TODO El tipo de retorno de los metodos puede ajustarse segun la conveniencia
	/**
	 * Requerimiento 2B:  Definir una cuadricula regular de N columnas por M filas. que incluya las longitudes y latitudes dadas
	 * @param  lonMin: Longitud minima presente dentro de la cuadricula
	 * @param  lonMax: Longitud maxima presente dentro de la cuadricula
	 * @param  latMin: Latitud minima presente dentro de la cuadricula
	 * @param  latMax: Latitud maxima presente dentro de la cuadricula
	 * @param  columnas: Numero de columnas de la cuadricula
	 * @param  filas: Numero de filas de la cuadricula
	 */
	public ArregloDinamico<String> definirCuadriculaB2(double lonMin, double lonMax, double latMin, double latMax, int columnas,
			int filas) 
	{
		// TODO Auto-generated method stub
		CordVO min=new CordVO(latMin,lonMin);
		CordVO max=new CordVO(latMax,lonMax);

		// Operaciones sobre el numero para dejar solo 4 decimales paso a paso
		// Numero original - 2.8049999999999997
		// 1. Multiplicamos por 100 (10^4) - 280.5
		// 2. Redondeamos - 281
		// 3. Dividimos por 100 (10^4) - 2.81

		Double difLongitudes=(lonMax-lonMin)/(columnas-1);
		difLongitudes = Math.round(difLongitudes * 10000) / 10000d;

		Double difLatitudes=(latMax-latMin)/(filas-1);
		difLatitudes = Math.round(difLatitudes * 10000) / 10000d;

		int numIntersecciones=filas*columnas;

		Double[] arregloLat=new Double[numIntersecciones];
		Double[] arregloLon=new Double[numIntersecciones];

		//Ciclo para guardar todas las coordenadas en los dos arreglos
		Double tmpLatitudes=0.0;
		Double tmpLongitudes=0.0;
		Double sumaLon=0.0;
		Double sumaLat=0.0;
		int contIndice=0;
		for (int i = 0; i < (numIntersecciones/columnas); i++) 
		{
			tmpLatitudes+=difLatitudes*(i);
			tmpLongitudes=0.0;
			for (int j = 0; j < (numIntersecciones/filas); j++) 
			{	
				tmpLongitudes+=difLongitudes*(j);
				sumaLat=latMin+tmpLatitudes;
				sumaLat = Math.round(sumaLat * 10000) / 10000d;
				sumaLon=lonMin+tmpLongitudes;
				sumaLon = Math.round(sumaLon * 10000) / 10000d;
				arregloLon[contIndice]=sumaLon;
				arregloLat[contIndice]=sumaLat;
				contIndice++;
			}
		}

		ArregloDinamico<CordVO> coordenadasMatriz=new ArregloDinamico<CordVO>(numIntersecciones);

		//Ciclo obtener un Arreglo de con las coordenadas de la matriz sobre un area de interes
		for (int i = 0; i < numIntersecciones; i++) 
		{
			CordVO x=new CordVO(arregloLat[i], arregloLon[i]);
			coordenadasMatriz.agregar(x);
		}

		//Compara con los vertices del Grafo y da un arreglo de los vertices mas cercanos de el

		ArregloDinamico<String> llavesGrafo=grafoVias.keys();
		ArregloDinamico<String> verticesAprox=new ArregloDinamico<String>(numIntersecciones);

		for (int i = 0; i < numIntersecciones; i++) 
		{
			CordVO x=coordenadasMatriz.darElemento(i);
			String aprox=null;
			Double menorDist=Double.MAX_VALUE;
			for (int j = 0; j < llavesGrafo.darTamano(); j++) //For despiadado
			{
				CordVO y=grafoVias.getInfoVertex(llavesGrafo.darElemento(j));
				Double dist=calcularDistanciaHarversiana(x, y);
				if(dist<menorDist)
				{
					aprox=llavesGrafo.darElemento(j);
					menorDist=dist;
				}
			}

			verticesAprox.agregar(aprox);
		}

		return verticesAprox;

	}

	// TODO El tipo de retorno de los metodos puede ajustarse segun la conveniencia
	/**
	 * Requerimiento 1C:  Calcular un arbol de expansion minima (MST) con criterio distancia, utilizando el algoritmo de Kruskal.
	 */
	public GrafoNoDirigido<String, CordVO, pesoWay> arbolMSTKruskalC1(GrafoNoDirigido<String, CordVO, pesoWay> grafoRepreMapa) {
		// TODO Auto-generated method stub
		Kruskal al = new Kruskal();
		GrafoNoDirigido<String, CordVO, pesoWay> krus = al.aplicarKruskal(grafoRepreMapa,"");
		return krus;
	}

	// TODO El tipo de retorno de los metodos puede ajustarse segun la conveniencia
	/**
	 * Requerimiento 2C: Calcular un arbol de expansion minima (MST) con criterio distancia, utilizando el algoritmo de Prim. (REQ 2C)
	 */
	public GrafoNoDirigido<String, CordVO, pesoWay> arbolMSTPrimC2() 
	{
		// TODO Auto-generated method stub
		Prim<String, CordVO, pesoWay> pr= new Prim<String, CordVO, pesoWay>(grafoVias);
		GrafoNoDirigido<String, CordVO, pesoWay> grafoVias2 = new GrafoNoDirigido<String, CordVO, pesoWay>();
		Iterable<Arco<String,CordVO,pesoWay>>x =pr.edges();
		Iterator<Arco<String,CordVO,pesoWay>> it=x.iterator();
		while(it.hasNext()) {
			Arco<String,CordVO,pesoWay> arc = it.next();	    	
			grafoVias2.addVertex(arc.getInicial().getId(), arc.getInicial().getInfo());
			grafoVias2.addVertex(arc.getFinal().getId(), arc.getFinal().getInfo());
			grafoVias2.addEdge(arc.getInicial().getId(), arc.getFinal().getId(), arc.getInfoArco());
		}
		return grafoVias2;
	}

	// TODO El tipo de retorno de los metodos puede ajustarse segun la conveniencia
	/**
	 * Requerimiento 3C: Calcular los caminos de costo minimo con criterio distancia que conecten los vertices resultado
	 * de la aproximacion de las ubicaciones de la cuadricula N x M encontrados en el punto 5.
	 */
	public void caminoCostoMinimoDijkstraC3(ArregloDinamico<String> x) 
	{
		// TODO Auto-generated method stub
		HashSeparateChaining<String,Integer> interprete=grafoVias.getTablaI();
		Queue<Arco<String, CordVO, pesoWay>>[] param=darAdyacentesNumericos();
		for (int i = 0; i < x.darTamano(); i++) 
		{
			djPorDistancia=new DijkstraPorDistancia(grafoVias, interprete.get(x.darElemento(i)+""), param);
			
			CordVO inicio =param[i].dequeue().getInicial().getInfo();
			CordVO fin =  param[i].dequeue().getFinal().getInfo();
			System.out.println("CordVerticeInicial: "+inicio.toString());
			System.out.println("CordVerticeFinal: "+fin.toString());
			Arco<String, CordVO, pesoWay>jo = param[i].dequeue();
			for(Arco<String, CordVO, pesoWay> g: jo)
        	{
        		System.out.println("Arcos de los vertices");
        		System.out.println(g.getInfoArco().toString());
        	}
		}
		
		
		

	        	for(Arco<String, CordVO, pesoWay> g: jo)
	        	{
	        		System.out.println("Arcos de los vertices");
	        		System.out.println(g.getInfo().toString());
	        	}
	        

	}
	// TODO El tipo de retorno de los metodos puede ajustarse segun la conveniencia
	/**
	 * Requerimiento 4C:Encontrar el camino mas corto para un viaje entre dos ubicaciones geograficas escogidas aleatoriamente al interior del grafo.
	 * @param idVertice2 
	 * @param idVertice1 
	 */
	public void caminoMasCortoC4(int idVertice1, int idVertice2) 
	{
		// TODO Auto-generated method stub
		HashSeparateChaining<String,Integer> interprete=grafoVias.getTablaI();
		BFS<String, CordVO, pesoWay> bfs=new BFS<String, CordVO, pesoWay>(grafoVias, interprete.get(idVertice1+""));
	}
	/**
	 * Crea y grafica el mapa 
	 * @param argumentos
	 */
	public void generarMapaGrafo(GrafoNoDirigido<String, CordVO, pesoWay> pGrafo,LatLng min,LatLng max,boolean pSinoVertices, GrafoNoDirigido<String, CordVO, pesoWay> resaltar)
	{
		Mapa x=new Mapa(pGrafo,min,max,pSinoVertices, resaltar);
		Mapa.graficarMapa(x);
	}	

	/**
	 * Cargar las infracciones de un semestre de 2018
	 * @param numeroSemestre numero del semestre a cargar (1 o 2)
	 * @return objeto con el resultado de la carga de las infracciones
	 */
	@SuppressWarnings("deprecation")
	public void loadMovingViolations() 
	{
		// TODO Realizar la carga de infracciones del semestre
		numTotalInfrac=0;
		//Elimina todo rastro de las estructuras anterior
		Runtime garbage = Runtime.getRuntime();
		garbage.gc();

		int numMesesCargados=0;
		int infracPorMes[]=new int[12];

		try 
		{
			int x=0;
			int y=0;
			int numTotalInfracciones=0;

			for (int i = 0; i < 12; i++) 
			{
				String xMes=listaMes[x];
				CSVReader csvReader = new CSVReader(new FileReader("."+File.separator+"data"+File.separator+xMes+"_wgs84.csv"),';');
				String[] fila=null;
				csvReader.readNext();
				int numInfrMes=0;

				while ((fila=csvReader.readNext()) != null) 
				{
					VOMovingViolations infraccion;
					//Crea la infraccion.
					if(xMes.equals("January"))
					{
						infraccion=new VOMovingViolations(fila[0], fila[15], fila[2], fila[9], fila[12],convertirFecha_Hora_LDT(fila[13]), fila[14], fila[8], fila[4],fila[3], fila[10],fila[11],fila[17],fila[18]);
					}
					else if(xMes.equals("October")||xMes.equals("November")||xMes.equals("December"))
					{
						infraccion=new VOMovingViolations(fila[1], fila[17], fila[3],fila[10], fila[14], convertirFecha_Hora_LDT(fila[15]),fila[16],fila[9], fila[5],fila[4], fila[11],fila[13],fila[19],fila[20]);
					}
					else 
					{
						infraccion=new VOMovingViolations(fila[1], fila[16], fila[3],fila[10], fila[13], convertirFecha_Hora_LDT(fila[14]),fila[15],fila[9], fila[5],fila[4], fila[11],fila[12],fila[18],fila[19]);
					}

					//Agrega las infracciones a la tabla de infracciones 

					tablaInfracciones.put(infraccion.getObjectId(), infraccion);

					numInfrMes++;		
				}
				numTotalInfracciones+=numInfrMes;
				infracPorMes[y]=numInfrMes;
				y++;
				x++;
				numMesesCargados++;
				csvReader.close();
				System.out.println("Infracciones del mes :" +xMes +" ::: "+ numInfrMes);
			} 
			numTotalInfrac=numTotalInfracciones;
			System.out.println("Total de Infracciones del a�o 2018:: "+numTotalInfrac);
			//Crea la clase de estadisticas
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha);
	}
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}
	/**
	 * Retorna los arcos de una cola pasada por parametro en un arreglo.
	 * @return
	 */
	private Arco<String, CordVO, pesoWay>[] getArregloArcos(Queue<Arco<String, CordVO, pesoWay>> cola)
	{
		IteratorQueue<Arco<String, CordVO, pesoWay>> it=(IteratorQueue<Arco<String, CordVO, pesoWay>>) cola.iterator();
		Arco<String, CordVO, pesoWay>[] infracciones=new Arco[cola.size()];
		int i=0;
		while(it.hasNext())
		{
			Arco<String, CordVO, pesoWay> x=it.next();
			infracciones[i]=x;
			i++;
		}
		return infracciones;
	}

	/*
	 * Calcula la distancia harversiana a partir de 2 cordVO
	 * Adaptado de::https://joseguerreroa.wordpress.com/2012/11/13/distancia-entre-dos-puntos-de-la-superficie-terrestre-mediante-la-formula-de-haversine-con-python/
	 */
	private static Double calcularDistanciaHarversiana(CordVO x,CordVO y)
	{
		//Operacion completa dist Harvesiana::
		//rta = 2*RadioTierra*asin(sqrt(sin(constanteRad*(lat2-lat1)/2)**2 + cos(c*lat1)*cos(c*lat2)*sin(c*(long2-long1)/2)**2))
		Double lat1=x.getLat();
		Double lat2=y.getLat();
		Double long1=x.getLong();
		Double long2=y.getLong();

		Double rta=(double) 0;

		Double latDif=(lat2-lat1)/2; //(lat2-lat1)/2)

		Double sinlat=Math.pow(Math.sin(constanteRad*latDif),2); //sin(constanteRad*(lat2-lat1)/2)**2

		Double longDif=(constanteRad*(long2-long1))/2; //(c*(long2-long1)/2)

		Double sinLong=Math.pow(Math.sin(longDif),2); //sin(c*(long2-long1)/2)**2)

		Double cosenos=(Math.cos(constanteRad*lat1))*(Math.cos(constanteRad*lat2));//cos(c*lat1)*cos(c*lat2)

		Double sumatoria=sinlat+(cosenos*sinLong); //sin(constanteRad*(lat2-lat1)/2)**2 + cos(c*lat1)*cos(c*lat2)*sin(c*(long2-long1)/2)**2)

		Double raiz=Math.sqrt(sumatoria); //sqrt(sin(constanteRad*(lat2-lat1)/2)**2 + cos(c*lat1)*cos(c*lat2)*sin(c*(long2-long1)/2)**2)

		Double arcSin=Math.asin(raiz); //asin(sqrt(sin(constanteRad*(lat2-lat1)/2)**2 + cos(c*lat1)*cos(c*lat2)*sin(c*(long2-long1)/2)**2))

		rta= (2*RadioTierra*arcSin); //rta = 2*RadioTierra*asin(sqrt(sin(constanteRad*(lat2-lat1)/2)**2 + cos(c*lat1)*cos(c*lat2)*sin(c*(long2-long1)/2)**2))

		return rta;
	}

	private double darDist(Queue<Arco<String,CordVO,pesoWay>> cola) {
		double ret = 0;
		for(int i = 0; i < cola.darTamano(); i++) {
			Arco<String, CordVO, pesoWay> arc = cola.dequeue();
			Vertice<String, CordVO, pesoWay> vert1 = arc.getInicial();
			Vertice<String, CordVO, pesoWay> vert2 = arc.getFinal();
			ret += calcularDistanciaHarversiana(vert1.getInfo(), vert2.getInfo());				
		}
		return ret;
	}
	/**
	 * Retorna un arreglo con todos los vertices adyacentes de un indice numerico
	 * @return
	 */
	public Queue<Arco<String, CordVO, pesoWay>>[] darAdyacentesNumericos()
	{
		HashSeparateChaining<Integer, String> tablaRI=grafoVias.getTablaRI();
		Queue<Arco<String, CordVO, pesoWay>>[] rta=(Queue<Arco<String, CordVO, pesoWay>>[]) new Queue[grafoVias.V()];
		for (int i = 0; i < grafoVias.V(); i++) 
		{	
			String id=tablaRI.get(i);
			Vertice<String, CordVO, pesoWay> x=grafoVias.darVertice(id);
			Queue<Arco<String, CordVO, pesoWay>> y=x.darAdyacentes();
			rta[i]=y;
		}
		return rta;
	}

	//	/**
	//	 * 
	//	 * @author na.tobo
	//	 *
	//	 */
	//
	//	public class comparatorVertex implements Comparator<Vertice<String, CordVO, pesoWay>>
	//	{
	//
	//		public int compare(Vertice<String, CordVO, pesoWay>o1, Vertice<String, CordVO, pesoWay> o2) {
	//			// TODO Auto-generated method stub
	//			return o1.getInfo().getColaInfracciones().size() - o2.getInfo().getColaInfracciones().size();
	//		}
	//
	//	}

}