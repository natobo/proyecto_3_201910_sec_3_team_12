package model.data_structures;

import model.vo.CordVO;
import model.vo.pesoWay;
//recuperado de https://algs4.cs.princeton.edu/code/edu/princeton/cs/algs4/PrimMST.java.html
public class Prim <K extends Comparable<K>,V,A>
{
	private static final double FLOATING_POINT_EPSILON = 1E-12;
	private static final double INFINITO = Integer.MAX_VALUE; //Constante para marcar los vertices con el infinito
    private HashSeparateChaining<Integer, Arco<String,CordVO,pesoWay>> arcoTo;        // arcoTo[v] = shortest edge from tree vertex to non-tree vertex
    private HashSeparateChaining<Integer, Double> distTo;      // distTo[v] = weight of shortest such edge
    private HashSeparateChaining<Integer, Boolean> marked;     // marked[v] = true if v on tree, false otherwise
    private IndexMinPQ<Double> pq;
    private HashSeparateChaining<Integer, String> traductorRI;
    private HashSeparateChaining<String, Integer> traductorI; 
    /**
     * Compute a minimum spanning tree (or forest) of an edge-weighted graph.
     * @param G the edge-weighted graph
     */
    public Prim(GrafoNoDirigido<String, CordVO, pesoWay> G) 
    {
        arcoTo = new HashSeparateChaining<Integer, Arco<String,CordVO,pesoWay>>(G.V());
        distTo = new HashSeparateChaining<Integer, Double>(G.V());
        marked = new HashSeparateChaining<Integer, Boolean>(G.V());
        traductorRI = G.getTablaRI();
        traductorI = G.getTablaI();
        pq = new IndexMinPQ<Double>(G.V());


             // minimum spanning forest

        // check optimality conditions
    }

    // run Prim's algorithm in graph G, starting from vertex s
    private void prim(GrafoNoDirigido<String, CordVO, pesoWay> G, int s)
    {
    	for(int i = 0; i<G.E();i++) {
    		distTo.put(i,INFINITO);
    	}
    	for (int v = 0; v < G.V(); v++) {      // run from each vertex to find
            if (!marked.get(v)) prim(G, v); 
    	}
        distTo.put(s, 0.0);
        pq.insert(s, distTo.get(s));
        while (!pq.isEmpty())
        {
            int v = pq.delMin();
            scan(G, v);
        }
    }

    // scan vertex v
    private void scan(GrafoNoDirigido<String, CordVO, pesoWay> G, int v) 
    {
        marked.put(v, true);
        
        Vertice<String, CordVO, pesoWay> vertex = G.darVertice(traductorRI.get(v));
        Queue<Arco<String, CordVO, pesoWay>> arcos = vertex.darAdyacentes();
        
        for (Arco<String,CordVO,pesoWay> e : arcos) 
        {
            int w = traductorI.get(e.getFinal().getId());
            if (marked.get(w)) continue;         // v-w is obsolete edge
            if (e.getInfoArco().getDistHarvesiana() < distTo.get(w)) 
            {
                distTo.put(w, e.getInfoArco().getDistHarvesiana());
                arcoTo.put(w, e);
                if (pq.contains(w)) pq.decreaseKey(w, distTo.get(w));
                else                pq.insert(w, distTo.get(w));
            }
        }
    }

    /**
     * Returns the edges in a minimum spanning tree (or forest).
     * @return the edges in a minimum spanning tree (or forest) as
     *    an iterable of edges
     */
    public Iterable<Arco<String,CordVO,pesoWay>> edges() 
    {
        Queue<Arco<String,CordVO,pesoWay>> mst = new Queue<Arco<String,CordVO,pesoWay>>();
        for (int v = 0; v < arcoTo.numKeys(); v++) {
            Arco<String,CordVO,pesoWay> e = arcoTo.get(v);
            if (e != null) {
                mst.enqueue(e);
            }
        }
        return mst;
    }

    /**
     * Returns the sum of the edge weights in a minimum spanning tree (or forest).
     * @return the sum of the edge weights in a minimum spanning tree (or forest)
     */
    public double weight() {
        double weight = 0.0;
        for (Arco<String,CordVO,pesoWay> e : edges())
            weight += e.getInfoArco().getDistHarvesiana();
        return weight;
    }


    
}
