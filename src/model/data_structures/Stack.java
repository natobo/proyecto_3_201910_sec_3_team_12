package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class Stack<T> implements IStack<T>
{
	/**
	 * Arreglo dinamico de los elementos de la pila 
	 */
	private ArrayList<T> pila;
	/**
	 * Contador de elementos.
	 */
	private int N = 0;
	/**
	 * Contructor de la clase donde se inicaliza el arreglo dinamico.
	 */
	public Stack()
	{
		pila = new ArrayList<T>();
	}
	
	@Override
	public  Iterator<T> iterator() 
	{
		// TODO Auto-generated method stub
		return pila.iterator();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return N == 0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return pila.size();
	}

	@Override
	public void push(T t) {
		// TODO Auto-generated method stub
		pila.add(t);
		N++;
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		T x = pila.get(N-1);
		pila.remove(N-1);
		N--;
		return x;
	}
	

}
