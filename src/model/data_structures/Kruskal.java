package model.data_structures;

import model.vo.CordVO;
import model.vo.pesoWay;

public class Kruskal 
{
	
	
	
	@SuppressWarnings("unchecked")
	public GrafoNoDirigido<String, CordVO, pesoWay> aplicarKruskal(GrafoNoDirigido<String, CordVO, pesoWay> grafo, String tipo)
 	{
 		GrafoNoDirigido<String, CordVO, pesoWay> �rbol = new GrafoNoDirigido<String, CordVO, pesoWay>();
 		ArregloDinamico<String> nodos = grafo.keys();
 
 		for(int j = 0; j < nodos.darTamano(); j++)
 		{
 			String idVert = nodos.darElemento(j);
 			�rbol.addVertex(idVert, (CordVO) grafo.getInfoVertex(idVert));
 		}
 

  		MinPQ<Arco<String, CordVO, pesoWay>> L = grafo.darMPQArcos(tipo);
 
  		Arco<String, CordVO, pesoWay> pro = L.delMin();
 		�rbol.addEdge(pro.getInicial().getId(), pro.getFinal().getId(), pro.getInfoArco());
 		
 
 		while(L.size()!=0)
 		{
 			pro = L.delMin();
 
 			if(HayCiclo(�rbol, pro,�rbol.darVertice(pro.getFinal().getId()) , pro.getFinal().getId())==false)
 				�rbol.addEdge(pro.getInicial().getId(), pro.getFinal().getId(), pro.getInfoArco());
 
 			
 		}
 
 		return �rbol;
 	}
 
 	private boolean HayCiclo(GrafoNoDirigido<String, CordVO, pesoWay> g,Arco<String, CordVO, pesoWay> aVerificar,Vertice<String, CordVO, pesoWay> terminal,String N)
 	{
 		Queue<Arco<String, CordVO, pesoWay>> aux = terminal.getAdyacentes();
 
 		if(aux.darTamano() == 0)
 			return false;
 
 		if(terminal.existeEnlace(aVerificar.getInicial().getId()) != -1)
 			return true;
 
 		for(int i=0;i< aux.darTamano();i++)
 		{
 			Arco<String, CordVO, pesoWay> nodo = aux.dequeue();
 
 			if(nodo.getFinal().getId().equals(N) == false)
 				if( HayCiclo(g,aVerificar,g.darVertice(nodo.getFinal().getId()),terminal.getId()))
 									return true;
 		}
 
 		return false;
 	}
}
