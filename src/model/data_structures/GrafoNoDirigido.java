package model.data_structures;

/**
 * 
 * Clase que representa un grafo no dirigido 
 * @author nicot
 *
 * @param <K> Llaves de los vertices
 * @param <V> Valor a guardar por los vertices
 * @param <A> Peso, o informacion de los Arcos
 */
public class GrafoNoDirigido<K extends Comparable<K>,V,A> implements IGrafoNoDirigido <K,V,A> 
{
	/**
	 * Cantidad de vertices 
	 */
	private int numVertices;
	/**
	 * Cantidad de Arcos
	 */
	private int numArcos;	
	/**
	 * Tabla de hash que contiene los vertices
	 */
	private HashSeparateChaining<K, Vertice<K, V,A>> verticesGrafo;
	/**
	 * Tabla de Hash que retorna un entero a partir de una llave
	 */
	private HashSeparateChaining<K, Integer> tablaI;
	/**
	 * Tabla de Hash que retorna una llave a partir de un entero.
	 */
	private HashSeparateChaining<Integer,K> tablaRI;
	/**
	 * Arreglo que representa todas las llaves de los vertices del grafo
	 */
	private ArregloDinamico<K> keys;
	/**
	 * Cola que guarda todos los arcos del grafo
	 */
	private Queue<Arco<K,V,A>> arcos; 
	
	private int dist =0; // para el c1
	/**
	 *Constructor de grafo no dirigido. 
	 */
	public GrafoNoDirigido() 
	{
		// TODO Auto-generated constructor stub
		verticesGrafo=new HashSeparateChaining<K, Vertice<K,V,A>>(745747);
		numArcos=0;
		numVertices=0;
		keys = new ArregloDinamico<K>(745747);
		arcos = new Queue<Arco<K,V,A>>();
		tablaI=new HashSeparateChaining<K, Integer>(745747);
		tablaRI=new HashSeparateChaining<Integer,K>(745747);
	}
	/**
	 * Retorna la tabla Hash de tipo integer
	 * @return
	 */
	public HashSeparateChaining<K, Integer> getTablaI() 
	{
		return tablaI;
	}
	/**
	 * Define la tabla Hash de tipo integer
	 * @return
	 */
	public void setTablaI(HashSeparateChaining<K, Integer> tablaI)
	{
		this.tablaI = tablaI;
	}
	/**
	 *  Retorna la tabla reversa de tipo integer
	 * @return
	 */
	public HashSeparateChaining<Integer, K> getTablaRI() 
	{
		return tablaRI;
	}
	/**
	 * Define la tabla reversa de tipo integer
	 * @param tablaRI
	 */
	public void setTablaRI(HashSeparateChaining<Integer, K> tablaRI) 
	{
		this.tablaRI = tablaRI;
	}
	/**
	 * Crea las tablas de index de integer a k y viceversa
	 */
	public void crearTablas()
	{
		int contador=0;
		for (int i = 0; i < keys.darTamano(); i++) 
		{
			tablaRI.put(contador,keys.darElemento(i));
			tablaI.put(keys.darElemento(i), contador);
			contador++;
		}
	}
	/**
	 * Retorna la cola con los arcos 
	 * @return queue arcos
	 */
	public Queue<Arco<K,V,A>> darArcos()
	{
		return arcos;
	}
	/**
	 * Retorna el arreglo de todos las llaves de los vertices del grafo 
	 * @return ArregloDinamico de las llaves del grafo.
	 */
	public ArregloDinamico<K> keys() 
	{
		return keys;
	}
	/**
	 * Retorna el vertice con el id pasado por parametro
	 * @param idVertex. Id del vertice a buscar en el grafo
	 * @return vertice dentro del grafo
	 */
	public Vertice<K,V,A> darVertice(K idVertex) 
	{
		return verticesGrafo.get(idVertex);
	}

	@Override
	public int V() 
	{
		// TODO Auto-generated method stub
		return numVertices;
	}

	@Override
	public int E() 
	{
		// TODO Auto-generated method stub
		return numArcos;
	}
	@Override
	public void addVertex(K idVertex, V infoVertex) 
	{
		// TODO Auto-generated method stub
		Vertice<K,V,A> x=new Vertice<K,V,A>(idVertex, infoVertex);
		verticesGrafo.put(idVertex, x);
		numVertices++;
		keys.agregar(idVertex);
	}
	@Override
	public void addVertexWithQueue(K idVertex, V infoVertex,Queue<K>pInfracciones,Queue<K> idVertices) 
	{
		// TODO Auto-generated method stub
		Vertice<K,V,A> x=new Vertice<K,V,A>(idVertex, infoVertex);
		x.setIdVAdj(idVertices);
		x.setInfracciones(pInfracciones);
		verticesGrafo.put(idVertex, x);
		numVertices++;
		keys.agregar(idVertex);
	}
	@Override
	public V getInfoVertex(K idVertex) 
	{
		// TODO Auto-generated method stub
		//		System.out.println(verticesGrafo.contains(idVertex)+" id::"+ idVertex);
		return verticesGrafo.get(idVertex).getInfo();
	}
	@Override
	public void addEdge(K idVertexIni, K idVertexFin, A infoArc) 
	{
		// TODO Auto-generated method stub
		if(!hasEdge(idVertexIni, idVertexFin))
		{
			Vertice<K, V,A> pInicial=verticesGrafo.get(idVertexIni);
			Vertice<K, V,A> pFinal=verticesGrafo.get(idVertexFin);
			Arco<K, V, A> arco=new Arco<K,V,A>(pInicial, pFinal, infoArc);		
			pInicial.agregarArco(arco); //Error aqui estoy a�adiendo dos veces en la misma cola un arco osea A a B y B a A
			pFinal.agregarArco(arco);
			numArcos++;
			arcos.enqueue(arco);
		}
	}
	@Override
	public boolean hasEdge(K idVertexIni, K idVertexFin)
	{
		boolean rta=false;
		Vertice<K, V,A> verticeInicial=verticesGrafo.get(idVertexIni);
		Vertice<K, V,A> verticeFinal=verticesGrafo.get(idVertexFin);

		Queue<Arco<K,V,A>>arcosInicial=verticeInicial.getAdyacentes();
		IteratorQueue<Arco<K,V,A>> it1=arcosInicial.iterator();
		while (it1.hasNext()&&!rta)
		{
			Arco<K,V,A> arc1 = it1.next();
			K IdArcInicial=arc1.getInicial().getId();
			K IdArcFinal=arc1.getFinal().getId();
			if((idVertexIni.equals(IdArcInicial)&&idVertexFin.equals(IdArcFinal))||(idVertexIni.equals(IdArcFinal)&&idVertexFin.equals(IdArcInicial)))
			{
				rta=true;
			}
		}

		Queue<Arco<K,V,A>>arcosFinales=verticeFinal.getAdyacentes();
		IteratorQueue<Arco<K,V,A>> it2=arcosFinales.iterator();
		while (it2.hasNext()&&!rta)
		{
			Arco<K,V,A> arc2 = it2.next();
			K IdArcInicial=arc2.getInicial().getId();
			K IdArcFinal=arc2.getFinal().getId();
			if((idVertexIni.equals(IdArcInicial)&&idVertexFin.equals(IdArcFinal))||(idVertexIni.equals(IdArcFinal)&&idVertexFin.equals(IdArcInicial)))
			{
				rta=true;
			}
		}
		return rta;
	}
	/**
	 * Retorna un iterador de los arcos adyacentes a un vertice
	 * @param idVertice
	 * @return
	 */
	public IteratorQueue<Arco<K,V,A>> darAdyacentesVertice(K idVertice)
	{
		Vertice<K, V, A>v=verticesGrafo.get(idVertice);
		IteratorQueue<Arco<K,V,A>> rta=v.darAdyacentes().iterator();
		return rta;
	}
	@Override
	public void setInfoVertex(K idVertex, V infoVertex) 
	{
		// TODO Auto-generated method stub
		verticesGrafo.get(idVertex).setInfo(infoVertex);
	}
	@Override
	public A getInfoArc(K idVertexIni, K idVertexFin) 
	{
		// TODO Auto-generated method stub
		A rta=null;
		Queue<Arco<K, V, A>> adjInicial=verticesGrafo.get(idVertexIni).darAdyacentes();
		IteratorQueue<Arco<K,V,A>> it=adjInicial.iterator();
		while (it.hasNext()&&rta==null) 
		{
			Arco<K, V, A> arco = (Arco<K, V, A>) it.next();
			if(arco.getFinal().equals(verticesGrafo.get(idVertexFin)))
			{
				rta=arco.getInfoArco();
				break;
			}
		}

		Queue<Arco<K, V, A>> adjFinal=verticesGrafo.get(idVertexFin).darAdyacentes();
		IteratorQueue<Arco<K,V,A>> it2=adjFinal.iterator();
		while (it2.hasNext()&&rta==null) 
		{
			Arco<K, V, A> arco = (Arco<K, V, A>) it2.next();
			if(arco.getFinal().equals(verticesGrafo.get(idVertexIni)))
			{
				rta=arco.getInfoArco();
				break;
			}
		}
		return rta;
	}

	@Override
	public void setInfoArc(K idVertexIni, K idVertexFin, A infoArc) 
	{
		// TODO Auto-generated method stub
		Queue<Arco<K, V, A>> adjInicial=verticesGrafo.get(idVertexIni).darAdyacentes();
		IteratorQueue<Arco<K,V,A>> it=adjInicial.iterator();
		while (it.hasNext()) 
		{
			Arco<K, V, A> arco = (Arco<K, V, A>) it.next();
			if(arco.getFinal().equals(verticesGrafo.get(idVertexFin))) //Duda si toca preguntar el inicial. arco.getInicial().equals(verticesGrafo.get(idVertexIni))&&
			{
				arco.setInfoArco(infoArc);
				break;
			}
		}
	}

	@Override
	public IteratorQueue<K> adj(K idVertex) 
	{
		// TODO Auto-generated method stub
		Queue<K> colaRta=new Queue<K>();
		Queue<Arco<K, V, A>> adjInicial=verticesGrafo.get(idVertex).darAdyacentes();
		IteratorQueue<Arco<K,V,A>> it=adjInicial.iterator();
		while (it.hasNext()) 
		{
			Arco<K, V, A> arco = (Arco<K, V, A>) it.next();
			colaRta.enqueue(arco.getFinal().getId());
		}
		return colaRta.iterator();
	}

	/**
	 * Retorna un arreglo con los valores numericos de las llaves adyacentes de un vertice.
	 * @param indiceInteger
	 * @return
	 */
	public ArregloDinamico<Integer> adjInteger(Integer indiceInteger)
	{
		ArregloDinamico<Integer> adj=new ArregloDinamico<Integer>(numVertices);

		K LlaveVertice=tablaRI.get(indiceInteger);

		Vertice<K, V, A> vertice=verticesGrafo.get(LlaveVertice);

		Queue<K> adyacentes=(Queue<K>) vertice.getIdVAdj();

		IteratorQueue<K> it=adyacentes.iterator();

		while (it.hasNext()) 
		{
			K idAdyacente=it.next();
			adj.agregar(tablaI.get(idAdyacente));
		}

		return adj;
	}
	/**
	 * Retorna el numero de vertices y el numero de Arcos del grafo
	 */
	public String toString() {
		StringBuilder s = new StringBuilder();

		s.append(numVertices + " vertices, " + numArcos + " arcos ");

		return s.toString();
	}
	
	public MinPQ<Arco<K,V,A>> darMPQArcos(String tipo) 

	{
		MinPQ<Arco<K,V,A>> min = new MinPQ<Arco<K,V,A>>(arcos.darTamano(),tipo);
		Queue<Arco<K,V,A>> copia = arcos;
		for(int i = 0; i < copia.darTamano(); i++) {
			min.insert(copia.dequeue());
		}
		return min;
	}
	

}
