package model.data_structures;


/**
 * 2019-01-23
 * Estructura de Datos Arreglo Dinamico de Strings.
 * El arreglo al llenarse (llegar a su maxima capacidad) debe aumentar su capacidad.
 * @author Fernando De la Rosa
 *
 */
public class ArregloDinamicoNC<T> 
{
		/**
		 * Capacidad maxima del arreglo
		 */
        private int tamanoMax;
		/**
		 * Numero de elementos en el arreglo (de forma compacta desde la posicion 0)
		 */
        private int tamanoAct;
        /**
         * Arreglo de elementos de tamaNo maximo
         */
        private T elementos[ ];

        /**
         * Construir un arreglo con la capacidad maxima inicial.
         * @param max Capacidad maxima inicial
         */
		public ArregloDinamicoNC( int max )
        {
               
               tamanoMax = max;
               elementos = (T[]) new Comparable[max];       
               tamanoAct = 0;
        }
        
		public void add( T dato )
        {
               if ( tamanoAct == tamanoMax )
               {  // caso de arreglo lleno (aumentar tamaNo)
                    tamanoMax = 2 * tamanoMax;
                    T [ ] copia = elementos;
                    elementos = (T[]) new Comparable[tamanoMax];  
                    for ( int i = 0; i < tamanoAct; i++)
                    {
                     	 elementos[i] = copia[i];
                    } 
//            	    System.out.println("Arreglo lleno: " + tamanoAct + " - Arreglo duplicado: " + tamanoMax);
               }
               
               elementos[tamanoAct++] = dato;
       }
		
		public int size() 
		{
			// TODO implementar
			return tamanoAct;
		}

		public T get(int i) 
		{
			// TODO implementar
			return elementos[i];
		}
		
		/**
		 * Elimina un dato del arreglo por el index que le pasan OJO: ESTE METODO NO AGRUPA LOS ELEMENTOS.
		 */
		public void eliminarPorPosicion(int index)
		{
		   elementos[index]=null;	
		}
		
		/**
		 * Intercambiar los datos de las posicion i y j
		 * @param datos contenedor de datos
		 * @param i posicion del 1er elemento a intercambiar
		 * @param j posicion del 2o elemento a intercambiar
		 */
		public void exchange( int i, int j)
		{
			// TODO implementar
			T tmp = elementos[i]; 
			elementos[i] = elementos[j]; 
			elementos[j] = tmp;
		}
		/**
		 * Elimina todo el arreglo creandolo de nuevo y liberando espacio en memoria con gc
		 */
		public void eliminarTodo()
		{
			elementos = (T[]) new Comparable[tamanoMax]; 
			tamanoAct = 0;
		}
		
		/**
	     * Retorna el numero de vertices y el numero de Arcos del graf
	     */
	    public String toString() {
	        StringBuilder s = new StringBuilder();
	        
	        s.append("Tama�o Array::"+size());
	       
	        return s.toString();
	    }
}
