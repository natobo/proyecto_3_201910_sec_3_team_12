package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IteratorMinColaP<T> implements Iterator<T>
{
	private NodoCola<T> proximo;

	public IteratorMinColaP( NodoCola<T> primero )
	{
		proximo = primero;
	}

	public boolean hasNext( )
	{
		return proximo != null;
	}

	public T next( )
	{
		if( proximo == null )
		{ 
			throw new NoSuchElementException("No hay proximo"); 
		}
		T elemento = proximo.darNodo(); 
		proximo = proximo.darSiguiente(); 
		return elemento;
	}
}
