package model.data_structures;

import java.util.Iterator;

public interface IHash <K extends Comparable<K>,V> 
{
	/**
	 * Agregar una dupla (K,V) a la	tabla. Si la llave K existe, se	 reemplaza	su	valor V	asociado.V no puede ser	null, si se llega a agregar una llave sin valor la elimina.
	 * @param key Llave
	 * @param value Valor
	 */
	public void put(K key,V value);
	/**
	 * Obtener el valor V asociado a la llave K.V no puede ser	null.
	 * @param key
	 * @return
	 */
	public V get(K key);
	/**
	 * Borrar la dupla asociada a la llave	K.	Se	obtiene	el	valor V	 asociado a la llave K. Se obtiene null si	la llave K no existe.
	 * @param key Llave del nodo a eliminar
	 * @return Valor asociado a la llave a eliminar
	 */
	public V delete(K key);
	/**
	 * Conjunto	de	llaves	K	presentes	en	la	tabla
	 * @return iterador de las llaves
	 */
	public Iterator<K>	keys();
	/**
	 * 	Retorna el tama�o de la tabla 
	 * @return int con el tama�o de la tabla
	 */
    public int size();
    /**
     * Retorna un booleano indicando si una tabla contiene una llave especifica.
     * @return True si encuentra la llave dentro de la tabla , False de lo contrario
     */
    public boolean contains(K key);
    /**
     * Retorna el numero de llaves de la tabla 
     */
    public int numKeys();
}
