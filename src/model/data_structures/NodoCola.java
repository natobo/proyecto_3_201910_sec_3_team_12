package model.data_structures;

public class NodoCola<T>
{

	// -----------------------------------------------------------------
	// Atributos
	// -----------------------------------------------------------------
	
	private T nodo;
	
	private NodoCola<T> siguiente;
	
	private NodoCola<T> anterior;
	
	public NodoCola (T dato)
	{
		nodo = dato;
	}
	
	public T darNodo()
	{
		return nodo;
	}
	
	public NodoCola<T> darSiguiente()
	{
		return siguiente;
	}
	
	public NodoCola<T> darAnterior()
	{
		return anterior;
	}
	
	public void cambiarSiguiente(NodoCola<T> dato)
	{
		siguiente = dato;
	}
	
	public void cambiarAnterior(NodoCola<T> dato)
	{
		anterior = dato;
	}
}