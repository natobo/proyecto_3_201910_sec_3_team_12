package model.data_structures;

import java.util.Iterator;

/**
 * Clase que una tabla de Hash implementada sobre la solucion de colisiones SeparateChaining 
 * @author nicot
 * @param <K> Tipo generico de la llave de la tabla 
 * @param <V> Tipo generico del valor de la tabla 
 * Referencias Codigo:: https://algs4.cs.princeton.edu/home/ (Pagina oficial del libro Algorithms, 4th Edition)
 * Adaptado de: https://algs4.cs.princeton.edu/34hash/SeparateChainingHashST.java.html
 */
public class HashSeparateChaining< K extends Comparable<K>,V> implements IHash<K,V>
{
	/**
	 *Arreglo que contiene los nodos de la tabla de hash
	 */
	private ListaNodoTabla<K,V>[] arregloListas;
	/**
	 *Numero de llaves en la tabla
	 */
	private int numLlaves;
	/**
	 *tama�o de la tabla
	 */
	private int tama�o;
	/**
	 * Atributo que indica el tama�o Final de la tabla;
	 */
	private int contRehash;
	/**
	 * Constructor de la clase HashSeparateChaining
	 * @param tama�o del arreglo inicial
	 */
	public HashSeparateChaining(int pTama�o) 
	{
		tama�o=pTama�o;
		arregloListas =(ListaNodoTabla<K,V>[])new ListaNodoTabla[tama�o];
		for (int i = 0; i < tama�o; i++)
			arregloListas[i] = new ListaNodoTabla<K,V>();
	}

	/**
	 * Metodo que se encarga de reacomodar la tabla de hash cuando supera su factor de carga maximo
	 * @param nuevoTam . Tama�o nuevo de la tabla a reacomodar
	 */
	private void resize(int nuevoTam) 
	{
		HashSeparateChaining<K,V> temp = new HashSeparateChaining<>(nuevoTam);
		for (int i = 0; i < tama�o; i++) 
		{
			for (K key : arregloListas[i].keys())
			{
				temp.put(key, arregloListas[i].darValor(key));
			}
		}
		this.tama�o  = temp.tama�o;
		this.numLlaves  = temp.numLlaves;
		this.arregloListas = temp.arregloListas;
		temp.contRehash=this.contRehash ;
	}

	@Override
	public void put(K key, V value) 
	{
		if (key == null) throw new IllegalArgumentException("La llave a agregar en la tabla es nula ");
		// TODO Auto-generated method stub
		if(value==null) 
		{
			delete(key);// Elimina la llave 
			return; //Mata al metodo 
		}

		int numHash=hash(key);
		if(!arregloListas[numHash].contiene(key))numLlaves++; //Aumenta el tama�o si no se sobreescribe el nodo.

		double fc= numLlaves/tama�o; //Factor de carga        

		if(fc>5)
		{
			contRehash++;
			resize(2*tama�o);//Duplica el tama�o de la tabla 
		}
		arregloListas[numHash].agregarNodo(key, value); //Dentro del metodo de agregar nodo se encarga de sobreescribir el nodo en caso de que tenga el mismo valor.

	}

	@Override
	public V get(K key) 
	{
		// TODO Auto-generated method stub
		if (key == null) throw new IllegalArgumentException("La llave a agregar en la tabla es nula ");
		return   arregloListas[hash(key)].darValor(key);
	}

	@Override
	public V delete(K key) 
	{
		// TODO Auto-generated method stub
		try 
		{
			int numHash = hash(key);
			V rta=arregloListas[numHash].darValor(key);
			if (rta!=null) tama�o--;
			arregloListas[numHash].eliminar(key);
			double fc= numLlaves/tama�o; //Factor de carga  
			// Reduce la tabla a la mitad si el numero de llaves es menor a dos veces el tama�o y el factor de carga es menor a la mitad del factor de carga que genera duplicacion. 
			if (fc<2.5&&numLlaves <= 2*tama�o) resize(tama�o/2);
			return rta;	
		} 
		catch (Exception e) 
		{
			// TODO: handle exception
			return null;
		}

	}

	@Override
	public Iterator<K> keys() 
	{
		// TODO Auto-generated method stub
		Queue<K> cola = new Queue<K>();
		for (int i = 0; i < tama�o; i++) 
		{
			for (K key : arregloListas[i].keys())
				cola.enqueue(key);
		}

		return  cola.iterator();
	}
	/**
	 * Realiza la funcion de hash,(obtiene un numero basado entre 0 y tama�o-1)
	 * @param key
	 * @return int del hash.
	 */
	private int hash(K key)   
	{  
		return (key.hashCode() & 0x7fffffff) % tama�o; 
	}

	@Override
	public int numKeys()
	{
		return numLlaves;
	}

	/**
	 * Retorna el numero de rehashes realizados.
	 * @return
	 */
	public int numRehash()
	{
		return contRehash;
	}

	@Override
	public int size() 
	{
		// TODO Auto-generated method stub
		return tama�o;
	}

	@Override
	public boolean contains(K key) 
	{
		// TODO Auto-generated method stub
		if (key == null) throw new IllegalArgumentException("La llave a buscar es nula");
		return get(key) != null;
	} 
}
