package model.data_structures;

import model.vo.CordVO;
import model.vo.pesoWay;

public class DFS 
{
	private HashSeparateChaining<Integer, Boolean> marked;   // marked[v] = true iff v is reachable from s
	private HashSeparateChaining<Integer, Integer> edgeTo;       // edgeTo[v] = last edge on path from s to v
    private final int s;       // source vertex

    /**
     * Computes a directed path from {@code s} to every other vertex in digraph {@code G}.
     * @param  G the digraph
     * @param  s the source vertex
     * @throws IllegalArgumentException unless {@code 0 <= s < V}
     */
    public DFS(GrafoNoDirigido<String, CordVO, pesoWay> G, int s) {
        marked = new HashSeparateChaining<Integer, Boolean>(G.V());
        edgeTo = new HashSeparateChaining<Integer, Integer>(G.V());
        this.s = s;
        validateVertex(s);
        dfs(G, s);

    }

    private void dfs(GrafoNoDirigido<String, CordVO, pesoWay> G, int v) { 
        marked.put(v, true);
        ArregloDinamico<Integer> adj= G.adjInteger(v);

		for (int w = 0; w < adj.darTamano(); w++) { 
            if (!marked.get(w)) {
                edgeTo.put(w, v);
                dfs(G, w);
            }
        }
    }

    /**
     * Is there a directed path from the source vertex {@code s} to vertex {@code v}?
     * @param  v the vertex
     * @return {@code true} if there is a directed path from the source
     *         vertex {@code s} to vertex {@code v}, {@code false} otherwise
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public boolean hasPathTo(int v) {
        validateVertex(v);
        return marked.get(v);
    }

    
    /**
     * Returns a directed path from the source vertex {@code s} to vertex {@code v}, or
     * {@code null} if no such path.
     * @param  v the vertex
     * @return the sequence of vertices on a directed path from the source vertex
     *         {@code s} to vertex {@code v}, as an Iterable
     * @throws IllegalArgumentException unless {@code 0 <= v < V}
     */
    public Iterable<Integer> pathTo(int v) {
        validateVertex(v);
        if (!hasPathTo(v)) return null;
        Stack<Integer> path = new Stack<Integer>();
        for (int x = v; x != s; x = edgeTo.get(x))
            path.push(x);
        path.push(s);
        return path;
    }

    // throw an IllegalArgumentException unless {@code 0 <= v < V}
    private void validateVertex(int v) {
        int V = marked.numKeys();
        if (v < 0 || v >= V)
            throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
    }
}
