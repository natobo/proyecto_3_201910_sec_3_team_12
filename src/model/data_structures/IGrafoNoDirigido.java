package model.data_structures;
/**
 * 
 * @author nicotobo 
 */
public interface IGrafoNoDirigido <K,V,A> 
{
	/**
	 * Retorna Numero de vertices del grafo
	 */
	public int V();
	/**
	 * Retorna el numero de arcos del grafo . Cada arco No dirigido debe contarse una única vez 
	 */
	public int E();
	/**
	 *  Adiciona un vértice con un Id único. El vértice tiene la información InfoVertex
	 */
	void addVertex( K idVertex, V infoVertex);
	/**
	 * Adiciona un vertice y sobreescribe las dos colas que le pasan por parametro
	 * @param idVertex
	 * @param infoVertex
	 * @param pInfracciones
	 * @param idVertices
	 */
	void addVertexWithQueue(K idVertex, V infoVertex,Queue<K>pInfracciones,Queue<K> idVertices) ;
	/**
	 * Obtener la información de un vértice
	 */
	V getInfoVertex(K idVertex) ;
	/**
	 * Adiciona el arco No dirigido entre el vertice IdVertexIni y el vertice
     * IdVertexFin. El arco tiene la información infoArc
	 * @param idVertexIni
	 * @param idVertexFin
	 * @param infoArc
	 */
	void addEdge(K idVertexIni, K idVertexFin, A infoArc );
	/**
	 * Revisa si ya existe un arco que conecta dos vertices dentro del grafo.
	 * @param idVertexIni
	 * @param idVertexFin
	 * @return  True si existe arco, False de lo contrario
	 */
    boolean hasEdge(K idVertexIni, K idVertexFin);
	/**
	 * Modificar la información del vértice idVertex
	 * @param idVertex
	 * @param infoVertex
	 */
	void setInfoVertex(K idVertex, V infoVertex);
	/**
	 * Obtener la información de un arco, si no existe el arco retorna null
	 * @param idVertexIni
	 * @param idVertexFin
	 * @return infoArco
	 */
	A getInfoArc(K idVertexIni, K idVertexFin);
	
	/**
	 * Modificar la información del arco entre los vértices idVertexIni e idVertexFin
	 * @param idVertexIni
	 * @param idVertexFin
	 * @param infoArc
	 */
	void setInfoArc(K idVertexIni, K idVertexFin,A infoArc);
	/**
	 * Retorna los identificadores de los vértices adyacentes a idVertex
	 * @param idVertex
	 * @return
	 */
	IteratorQueue<K> adj(K idVertex); 
}
