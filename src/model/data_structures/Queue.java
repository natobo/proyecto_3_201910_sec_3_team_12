package model.data_structures;

public class Queue<T>  implements IQueue<T> 
{

	/**
	 * Nodo con referencia al primer elemento a�adido a la cola
	 */
	private Node<T> primero;
	/**
	 * Nodo con referencia al ultimo elemento a�adido de la cola
	 */
	private Node<T> ultimo;
	/**
	 * Entero con el tama�o de la cola
	 */
	private int tamano;
	/**
	 * Constructor de la clase
	 */
	public Queue() 
	{
		// TODO Auto-generated constructor stub
		primero=null;
		ultimo=null;
		tamano=0;
	}

	@Override
	public IteratorQueue<T> iterator() 
	{
		// TODO Auto-generated method stub
		return new IteratorQueue<T>(primero);
	}

	@Override
	public boolean isEmpty() 
	{
		// TODO Auto-generated method stub
		return primero==null; //O puede ser tamano==0
	}

	@Override
	public int size() 
	{
		// TODO Auto-generated method stub
		return tamano;
	}

	@Override
	public void enqueue(T t) //A�ade un elemento nuevo al final de la cola
	{
		// TODO Auto-generated method stub
		Node<T> antiguoUltimo=ultimo; 
		ultimo = new Node<T>(t,null);                 
		if (isEmpty()) primero = ultimo;      
		else    antiguoUltimo.setSiguiente(ultimo); 
		tamano++; 
	}

	@Override
	public T dequeue() 
	{
		// TODO Auto-generated method stub
		T elemento=primero.getElemento();
		primero=primero.getSiguiente();
		if(isEmpty())ultimo=null;
		tamano--;
		return elemento;
	}

	@Override
	public T darPrimerElemento() 
	{
		// TODO Auto-generated method stub
		return primero.getElemento();
	}

	@Override
	public IQueue<T> unirColas(IQueue<T> colaAunir) 
	{
		// TODO Auto-generated method stub
		IQueue<T> unionColas=new Queue<T>();
		if(primero!=null&&ultimo!=null)
		{
			unionColas.setPrimero(primero);
			unionColas.setUltimo(ultimo);
			unionColas.darUltimo().setSiguiente(colaAunir.darPrimero());
			unionColas.setUltimo(colaAunir.darUltimo());
			unionColas.setTamano(colaAunir.darTamano()+tamano);
		}
		else
		{
			unionColas.setPrimero(colaAunir.darPrimero());
			unionColas.setUltimo(colaAunir.darUltimo());
			unionColas.setTamano(colaAunir.darTamano());
		}
		return unionColas;
	}

	@Override
	public Node<T> darPrimero() {
		// TODO Auto-generated method stub
		return primero;
	}

	@Override
	public Node<T> darUltimo() {
		// TODO Auto-generated method stub
		return ultimo;
	}

	@Override
	public void setPrimero(Node<T> pPrimero) 
	{
		// TODO Auto-generated method stub
		primero=pPrimero;	
	}

	@Override
	public void setUltimo(Node<T> pUltimo) 
	{
		// TODO Auto-generated method stub
		ultimo=pUltimo;	
	}

	@Override
	public int darTamano() {
		// TODO Auto-generated method stub
		return tamano;
	}

	@Override
	public void setTamano(int pTamano) {
		// TODO Auto-generated method stub
		tamano=pTamano;
	}

	@Override
	public void eliminarElemento(T pElemento)
	{
		// TODO Auto-generated method stub
		Queue<T> colaMitad2=new Queue<T>();
		int tamInicial=tamano;
        for (int i = 0; i < tamInicial; i++) 
        {
        	T element=this.dequeue();
        	if(element==pElemento)
        	{
        		colaMitad2.unirColas(this);
        		if(colaMitad2.darPrimero()!=null)
        		{
        			this.primero=colaMitad2.darPrimero();
        			if(this.darUltimo()==null)
        			{
        				this.ultimo=colaMitad2.darUltimo();
        			}
        		}
        		break;
        	}
        	else
        	{
        		colaMitad2.enqueue(element);
        	}
		}		
	}
}
