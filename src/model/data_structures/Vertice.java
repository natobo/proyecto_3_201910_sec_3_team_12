package model.data_structures;
/**
 * Clase que representa un vertice de la componente de los grafos 
 * @author nicotobo
 */

import model.vo.CordVO;
import model.vo.pesoWay;

public class Vertice<K extends Comparable<K>,V,A> implements Comparable<Vertice<K,V,A>>
{
	/**
	 * Llave que guarda el vertice
	 */
	private K id;
	/**
	 * Informacion que contiene el vertice
	 */
	private V info;
	/**
	 * Lista de Arcos con vertices adyacentes a este vertice 
	 */
    private Queue<Arco<K,V,A>> adyacentes;
    /**
     * Cola de ids de vertices adyacentes
     */
    private Queue<K> IdVAdj;
    /**
     * Cola que contiene las infracciones de vertices adyacentes
     */
    private Queue<K> infracciones;
    /**
     * Ar
     */
    private boolean visitado;
    /**
     * Vertice Anterior
     */
    private Vertice<K, V, A> anterior;
	/**
	 * Constructor del vertice 
	 * @param pLlave. Llave del vertice
	 * @param pValor.  Valor del vertice
	 */
	public Vertice(K pLlave, V pValor)
	{
		id=pLlave;
		info=pValor;
		adyacentes=new Queue<Arco<K,V,A>>();
		IdVAdj = new Queue<K>();
		infracciones = new Queue<K>();
		visitado = false;
	}
	/**
	 * Retorna la cola de arcos adyacentes del vertice
	 * @return
	 */
	public Queue<Arco<K, V, A>> getAdyacentes() {
		return adyacentes;
	}
	/**
	 * Define la cola de arcos adyacentes del vertice
	 * @param adyacentes
	 */
	public void setAdyacentes(Queue<Arco<K, V, A>> adyacentes) {
		this.adyacentes = adyacentes;
	}
	/**
	 * Retorna la cola de infracciones del vertice
	 * @return
	 */
	public Queue<K> getInfracciones() 
	{
		return infracciones;
	}
	/**
	 * Define la cola de infracciones del vertice
	 * @return
	 */
	public void setInfracciones(Queue<K> infracciones) {
		this.infracciones = infracciones;
	}
	/**
	 * Retorna la cola de id�s adyacentes del vertice
	 * @return
	 */
	public Queue<K> getIdVAdj() {
		return IdVAdj;
	}
	/**
	 * Retorna la llave o id del nodo
	 * @return id o llave
	 */
	public K getId() 
	{
		return id;
	}
	/**
	 * Define el id del vertice
	 * @param id o llave a definir
	 */
	public void setId(K id) 
	{
		this.id = id;
	}
	/**
	 * Retorna la informacion del vertice 
	 * @return info del vertice
	 */
	public V getInfo() 
	{
		return info;
	}
	/**
	 * Define la informacion del vertice pasada por parametri
	 * @param info 
	 */
	public void setInfo(V info) 
	{
		this.info = info;
	}
	/**
	 * A�ade un Arco de la lista de adyacentes de un vertice
	 * @param pArco vertice a a�adir en la lista de adyacentes
	 */
	public void agregarArco(Arco<K, V,A> pArco)
	{
		adyacentes.enqueue(pArco); 
	}
	/**
	 * Elimina un arco de la lista de adyacentes de un vertice 
	 * @param pArco vertice a eliminar de la lista
	 */
	public void eliminarArco(Arco<K,V,A> pArco)
	{
		adyacentes.eliminarElemento(pArco);
	}
	/**
	 * Retorna los arcos de la lista de adyacentes
	 */
	public Queue<Arco<K, V, A>> darAdyacentes()
	{
		return adyacentes;
	}
	/**
	 * Define la cola de id�s adyacentes del vertice
	 * @return
	 */
	public void setIdVAdj(Queue<K> pArco)
	{
		IdVAdj = pArco; 
	}
	
	public int darNumInf() 
	{
		return infracciones.darTamano();
	}
	
	public int existeEnlace(String enlazar)
 	{
		Queue<Arco<K,V,A>> copia = adyacentes;
 		for (int i = 0; i < copia.darTamano(); i++)
 		{
 			Arco<K,V,A> miEnlace = copia.dequeue();
 			if (miEnlace.getFinal().getId().equals(enlazar))
 				return 1;
 		}
 		return -1;
 	}
	
	public boolean isVisitado() {
		return visitado;
	}
	
	public void setVisitado(boolean b) {
		visitado = b;
	}
	@Override
	public int compareTo(Vertice<K, V, A> o) {
		// TODO Auto-generated method stub
		String s1 = darNumInf()+"";
		String s2 = o.darNumInf()+"";
		return s1.compareTo(s2);
	}
	/**
	 * Define el anterior de este vertice
	 * @param v
	 */
	public void setAnterior(Vertice<K, V, A> v)
	{
		anterior=v;
	}
	/**
	 * Retotna el anterior de este vertice
	 * @return
	 */
	public Vertice<K,V,A> getAnterior()
	{
		return anterior;
	}
	
	/**
	 * Retorna un Arco a partir de uno dado por parametro
	 * @param x
	 * @return
	 */
	public Arco<K,V,A> darArcoBuscado(Arco<K, V, A> x)
	{
		Arco<K,V,A> rta=null;
		IteratorQueue<Arco<K,V,A>> it=adyacentes.iterator();
		while (it.hasNext()&&rta==null) 
		{
			Arco<K, V, A> arco = (Arco<K, V, A>) it.next();
			if(arco.compareTo(x)==0)
			{
				rta=arco;
			}
		}
		
		return rta;
	}
	
	
	
}
