package model.data_structures;

/**
 *  Clase que representa la busqueda con BFS dentro del grafo
 *  Adaptado de :"https://algs4.cs.princeton.edu/41graph"
 */
public class BFS <K extends Comparable<K>,V,A>
{
	private static final int INFINITO = Integer.MAX_VALUE; //Constante para marcar los vertices con el infinito
	private HashSeparateChaining<Integer, Boolean> marked;  // Tabla  para determinar si esta marcado un vertices 
	private HashSeparateChaining<Integer, Integer> edgeTo;      // Tabla  para determinar el camino de un vertice("de donde viene")
	private HashSeparateChaining<Integer, Integer> distTo;      // Tabla  para determinar la distancia en arcos del vertice fuente

	/**
	 * Desarrolla el camino mas corto entre el vertice pasado por parametro y cualquier otro vertice del grafo
	 * @pre EL GRAFO DEBE TENER SUS TABLAS DE INTEGER INICIALIZADAS
	 * @param G grafo a analizar
	 * @param s el vertice inicial
	 * @throws IllegalArgumentException unless {@code 0 <= s < V}
	 */
	public BFS(GrafoNoDirigido<K, V, A> G, int s) 
	{
		marked = new HashSeparateChaining<Integer, Boolean>(G.V());
		distTo = new HashSeparateChaining<Integer, Integer>(G.V());
		edgeTo = new HashSeparateChaining<Integer, Integer>(G.V());
		validateVertex(s);
		bfs(G, s);
	}
	/**
	 * Realiza la busqueda de bfs a partir de un grafo
	 * @param G
	 * @param s
	 */
	//BFS a partir de un vertice
	private void bfs(GrafoNoDirigido<K, V, A> G, int s) 
	{
		Queue<Integer> q = new Queue<Integer>();    
		// recorremos todos los vértices del grafo inicializándolos a NO_VISITADO
		for (int v = 0; v < G.V(); v++)
		{
			distTo.put(v, INFINITO);  //En la tabla ubicamos todos los vertices a infinito
			marked.put(v,false); //Inicializamos todos en false
		}

		distTo.put(s, 0); 
		marked.put(s, true);
		q.enqueue(s);

		while (!q.isEmpty()) 
		{
			int v = q.dequeue();	

			ArregloDinamico<Integer> adj= G.adjInteger(v);

			for (int i = 0; i < adj.darTamano(); i++) 
			{
				Integer w=adj.darElemento(i);
				if (!marked.get(w)) 
				{
					edgeTo.put(w, v);
					distTo.put(w, distTo.get(v)+1);
					marked.put(w, true);
					q.enqueue(w);
				}
			}
		}
	}


	/**
	 * Existe un camino entre el vertice dado y el vertice pasado por parametro 
	 * @param v the vertex
	 * @return {@code true} if there is a path, and {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public boolean hasPathTo(int v) 
	{
		validateVertex(v);
		return marked.get(v);
	}

	/**
	 * Returns the number of edges in a shortest path between the source vertex {@code s}
	 * (or sources) and vertex {@code v}?
	 * @param v the vertex
	 * @return the number of edges in a shortest path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public int distTo(int v) 
	{
		validateVertex(v);
		return distTo.get(v);
	}

	/**
	 * Returns a shortest path between the source vertex {@code s} (or sources)
	 * and {@code v}, or {@code null} if no such path.
	 * @param  v the vertex
	 * @return the sequence of vertices on a shortest path, as an Iterable
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public Iterable<Integer> pathTo(int v) 
	{
		validateVertex(v);
		if (!hasPathTo(v)) return null;
		Stack<Integer> camino = new Stack<Integer>();
		int x;
		for (x = v; distTo.get(x) != 0; x = edgeTo.get(x))
			camino.push(x);
		camino.push(x);
		return camino;
	}
	
	/**
	 * Revisa que el vertice pasado por parametro si este dentro del rango de los vertices
	 * arroja una excepcion de lo contrario
	 * @param v
	 */
	private void validateVertex(int v) 
	{
		int V = marked.size();
		if (v < 0 || v >= V)
			throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
	}
}
