package model.data_structures;
/**
 * Clase que representa un arco del grafo 
 * @author nicot
 * @param <K> Llave de los vertices
 * @param <V> Valor de los vertices
 * @param <A> Informacion de los arcos
 */
public class Arco<K extends Comparable<K>,V,A> implements Comparable<Arco<K,V,A>> 
{
	/**
	 * Extremo 1 del arco
	 */
	private Vertice<K,V,A> vInicial;
	/**
	 * Extremo 2 del arco
	 */
	private Vertice<K,V,A> vFinal;
	/**
	 * Retorna la informacion del arco
	 */
	private A infoArco;
	/**
	 * Constructor del arco
	 * @param pX1 enlace 1
	 * @param pX2 enlace 2
	 * @param pInfo Informacion del arco
	 */
	public Arco(Vertice<K,V,A> pX1,Vertice<K,V,A> pX2,A pInfo){
		vInicial=pX1;
		vFinal=pX2;
		infoArco=pInfo;
	}
	/**
	 * Retorna el extremo1 del arco
	 * @return extremo1
	 */
	public Vertice<K,V,A> getInicial() {
		return vInicial;
	}
	/**
	 * Define el extremo1 del arco, , Ojo este metodo no refresca la lista de adyacentes del nodo
	 * @param extremo1
	 */
	public void setInicial(Vertice<K, V,A> extremo1) {
		this.vInicial = extremo1;
	}
	/**
	 * Retorna el extremo2 del arco
	 * @return extremo2
	 */
	public Vertice<K, V,A> getFinal() {
		return vFinal;
	}
	/**
	 * Define el extremo 2 del arco, Ojo este metodo no refresca la lista de adyacentes del nodo
	 * @param extremo2
	 */
	public void setFinal(Vertice<K, V,A> extremo2) {
		this.vFinal = extremo2;
	}
	/**
	 * Retorna la informacion del Arco (peso, kilometros, etc..) 
	 * @return informacion del arco
	 */
	public A getInfoArco() {
		return infoArco;
	}
	/**
	 * Define la informacion del arco.
	 * @param infoArco
	 */
	public void setInfoArco(A infoArco) {
		this.infoArco = infoArco;
	}
	/**
	 * Da el otro extremo dado por parametro
	 */
	public  Vertice<K,V,A> otroExtremo(K v)
	{
		Vertice<K,V,A> rta=null;
		if(vInicial.getId().equals(v))
			rta=vFinal;
		else
			rta=vInicial;
		return rta;
	}
	/**
	 * Cambia el sentido de Inicial A final
	 */
	public void swapVertices() 
	{
		Vertice<K,V,A> temp = vInicial;
		vInicial = vFinal;
		vFinal  = temp;
	}
	
	
	
	
	
	
	
	/**
	 * Retorna el numero de vertices y el numero de Arcos del graf
	 */
	public String toString() 
	{
		StringBuilder s = new StringBuilder();

		s.append("Vertice Inicial: "+"Coordenadas: "+"Vertice Final: "+vFinal.getId());

		return s.toString();
	}


	public int compareToInf(Arco<K, V, A> arc) {

		// TODO Auto-generated method stub
		int a1 = vInicial.darNumInf() + vFinal.darNumInf();
		int a2 = arc.getInicial().darNumInf() + arc.getFinal().darNumInf();
		String s1 = a1 + "";
		String s2 = a2 + "";
		return s1.compareTo(s2);
	}
	
	
	
	
	
	@Override
	public int compareTo(Arco<K, V, A> o) 
	{
		// TODO Auto-generated method stub
		if(this.getInicial()==o.getInicial() && this.getFinal()==o.getFinal())
			return 0;
		else if(this.getFinal()==o.getInicial() && this.getInicial()==o.getFinal())
			return 0;
		else
			return 1;
	}
}
