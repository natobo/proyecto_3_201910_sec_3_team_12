package model.data_structures;

import model.vo.CordVO;
import model.vo.pesoWay;
/**
 * Clase que representa el algoritmo de Dijkstra
 * @author na.tobo
 * Adaptado de:: https://algs4.cs.princeton.edu/44sp/DijkstraUndirectedSP.java.html
 * @param <K>Llaves
 * @param <V>
 */
public class DijkstraPorDistancia 
{
	private double[] distTo;          // distTo[v] = distance  of shortest s->v path
	private Arco<String, CordVO, pesoWay>[] edgeTo;            // edgeTo[v] = last edge on shortest s->v path
	private IndexMinPQ<Double> pq;    // priority queue of vertices
	public Queue<Arco<String, CordVO, pesoWay>>[] adj;
	private HashSeparateChaining<String,Integer> tablaI;
	private  HashSeparateChaining<Integer, String> tablaRI;
	private GrafoNoDirigido<String, CordVO, pesoWay> grafoVertices;


	public DijkstraPorDistancia (GrafoNoDirigido<String, CordVO, pesoWay> G, int s, Queue<Arco<String, CordVO, pesoWay>>[] param) 
	{
		Queue<Arco<String, CordVO, pesoWay>> cola = G.darArcos();
		while(!cola.isEmpty())
		{
			Arco<String, CordVO, pesoWay> e = cola.dequeue();
			if (e.getInfoArco().getDistHarvesiana() < 0)
				throw new IllegalArgumentException("edge " + e + " has negative weight");
		}
		adj = param;
		grafoVertices=G;
		distTo = new double[G.V()];
		edgeTo = (Arco<String, CordVO, pesoWay>[])new Arco[G.V()];
		tablaI=G.getTablaI();
		tablaRI=G.getTablaRI();
		validateVertex(s);

		for (int v = 0; v < G.V(); v++)
			distTo[v] = Double.POSITIVE_INFINITY;
		distTo[s] = 0.0;

		// relax vertices in order of distance from s
		pq = new IndexMinPQ<Double>(G.V());
		pq.insert(s, distTo[s]);
		while (!pq.isEmpty()) {
			int v = pq.delMin();
			Queue<Arco<String, CordVO, pesoWay>> adjuntos = adj[v];
			while(!adjuntos.isEmpty())
			{
				Arco<String, CordVO, pesoWay> e = adjuntos.dequeue();
				String idIn = e.getInicial().getId();
				relax(e, tablaI.get(idIn));
			}
		}
	}

	// relax edge e and update pq if changed
	private void relax(Arco<String, CordVO, pesoWay> e, int v) 
	{
		int w = tablaI.get(e.otroExtremo(tablaRI.get(v)).getId());
		if (distTo[w] > distTo[v] + e.getInfoArco().getDistHarvesiana()) 
		{
			distTo[w] = distTo[v] + e.getInfoArco().getDistHarvesiana();
			edgeTo[w] = e;
			if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
			else                pq.insert(w, distTo[w]);
		}
	}

	/**
	 * Returns the length of a shortest path between the source vertex {@code s} and
	 * vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @return the length of a shortest path between the source vertex {@code s} and
	 *         the vertex {@code v}; {@code Double.POSITIVE_INFINITY} if no such path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public double distTo(int v) {
		validateVertex(v);
		return distTo[v];
	}

	/**
	 * Returns true if there is a path between the source vertex {@code s} and
	 * vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @return {@code true} if there is a path between the source vertex
	 *         {@code s} to vertex {@code v}; {@code false} otherwise
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public boolean hasPathTo(int v) 
	{
		validateVertex(v);
		return distTo[v] < Double.POSITIVE_INFINITY;
	}

	/**
	 * Returns a shortest path between the source vertex {@code s} and vertex {@code v}.
	 *
	 * @param  v the destination vertex
	 * @return a shortest path between the source vertex {@code s} and vertex {@code v};
	 *         {@code null} if no such path
	 * @throws IllegalArgumentException unless {@code 0 <= v < V}
	 */
	public Iterable<Arco<String, CordVO, pesoWay>> pathTo(int v) {
		validateVertex(v);
		if (!hasPathTo(v)) return null;
		Stack<Arco<String, CordVO, pesoWay>> path = new Stack<Arco<String, CordVO, pesoWay>>();
		int x = v;
		for (Arco<String, CordVO, pesoWay> e = edgeTo[v]; e != null; e = edgeTo[x]) 
		{
			path.push(e);
			x = tablaI.get(e.otroExtremo(tablaRI.get(v)).getId());
		}
		return path;
	}


	// throw an IllegalArgumentException unless {@code 0 <= v < V}
	private void validateVertex(int v) {
		int V = distTo.length;
		if (v < 0 || v >= V)
			throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V-1));
	}

}