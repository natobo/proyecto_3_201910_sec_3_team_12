package model.vo;

import com.teamdev.jxmaps.LatLng;
import model.data_structures.Queue;

//vo ES VALUE OBJECT
public class CordVO implements Comparable<CordVO>
{
	/**
	 * Coordernada en x
	 */
	private Double Lat;
	/**
	 * Coordernada en y
	 */
	private Double Long;
	/**
	 * Coordenada de google
	 */
	private LatLng coord;
	/**
	 * Lista de infracciones que estan cercanas a esta coordenada
	 */
	private Queue<VOMovingViolations> colaInfracciones;
	/**
	 * Constructor de la clase
	 */
	public CordVO(Double pLat,Double pLong ) 
	{
		Lat=pLat;
		Long=pLong;
		coord=new LatLng(Lat,Long);
		colaInfracciones=new Queue<VOMovingViolations>();
	}

	/**
	 * Retorna la coordenada en X	
	 * @return
	 */
	public Double getLat() 
	{
		return Lat;
	}
	/**
	 * Retorna la coordenada en Y	
	 * @return
	 */
	public Double getLong() 
	{
		return Long;
	}
	/**
	 * Retorna la coordenada de LatLong
	 */
	public LatLng getLatLng()
	{
		return coord;
	}
	/**
	 *Retorna la cola de infracciones de la coordenada 
	 */
	public Queue<VOMovingViolations> getColaInfracciones()
	{
		return colaInfracciones;
	}
	/**
	 * Añade una infraccion a la cola de infracciones de la coordenada
	 */
	public void añadirInfraccion(VOMovingViolations pInfraccion)
	{
		colaInfracciones.enqueue(pInfraccion);
	}
	
	public LatLng getCoord() 
	{
		return coord;
	}

	public void setLatLng(LatLng coord) 
	{
		this.coord = coord;
	}

	public void setLat(Double lat) 
	{
		Lat = lat;
	}

	public void setLong(Double l)
	{
		Long = l;
	}

	public void setColaInfracciones(Queue<VOMovingViolations> colaInfracciones) 
	{
		this.colaInfracciones = colaInfracciones;
	}
	
	/**
	 * Compara dos LovationVo por su numero de registros si son iguales los compara por orden alfabetico de sus locaciones.
	 */
	@Override
	public int compareTo(CordVO o) 
	{
		// TODO Auto-generated method stub
		//Rta es positivo si el numero de registros es mayor al que me pasan por parametro.
		//Rta es negativo si el numeor de registros es menor al que me pasan por parametro.
		Integer x=this.getLatLng().hashCode();
		Integer y=o.getLatLng().hashCode();
		Integer rta=x.compareTo(y);
//		if(rta==0)
//		{
//			System.out.println(this.toString());
//			System.out.println("soyCero y no deberia serlo");
//		}
		return rta;
	}
	public String toString()
	{
		// TODO Convertir objeto en String (representacion que se muestra en la consola)
		return Lat+","+Long;
	}
}
