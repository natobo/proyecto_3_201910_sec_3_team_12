package model.vo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>
{

	/**
	 * objectId del auto
	 */
	private String objectId;
	/**
	 * violationDescription del auto
	 */
	private String violationDescription;
	/**
	 * totalPaid del auto
	 */
	private String totalPaid;
	/**
	 * accidentIndicator del auto.
	 */
	private String accidentIndicator;
	/**
	 * ticketIssueDate del auto.
	 */
	private LocalDateTime ticketIssueDate;
	/**
	 * violationCode del auto.
	 */
	private String violationCode;
	/**
	 * FineAMT de la infraccion.
	 */
	private String FineAMT;
	/**
	 * Direccion donde ocurrio la infraccion.
	 */
	private String Streetsegid ; 
	/**
	 * Address_Id de la infraccion.
	 */
	private String Address_Id;  
	/**
	 * Multa 1 de la infraccion
	 */
	private String penalty1;
	/**
	 * Multa 2 de la infraccion
	 */
	private String penalty2;
	/**
	 * Locacion de la infraccion
	 */
	private String location;
	/**
	 * Coordenanda en X de la infraccion.
	 */
	private String lat;
	/**
	 * Coordenanda en Y de la infraccion.
	 */
	private String lon;

	/**
	 * constructor
	 */
	public VOMovingViolations(String pObjectId,String pViolationDescription,String pLocation,String pTotalPaid,String pAccidentIndicator,LocalDateTime pTicketIssueDate,String pViolationCode,String pFineAMT,String pStreet, String pAddress_Id,String pPenalty1,String pPenalty2,String pLat,String pLong)
	{ 
		objectId=pObjectId;
		violationDescription=pViolationDescription;
		totalPaid=pTotalPaid;
		accidentIndicator=pAccidentIndicator;
		ticketIssueDate=pTicketIssueDate;
		violationCode=pViolationCode;
		FineAMT=pFineAMT;
		Streetsegid=pStreet;
		Address_Id=pAddress_Id;
		penalty1=pPenalty1;
		penalty2=pPenalty2;
		location=pLocation;
		lat=pLat;
		lon=pLong;
	}
	/**
	 * @return id - Identificador  unico de la infraccion
	 */
	public String getObjectId() 
	{
		// TODO Auto-generated method stub
		return objectId;
	}	

	/**
	 * @return location - Direccion en formato de texto.
	 */
	public String getLocation() 
	{
		// TODO Auto-generated method stub
		return location ;
	}

	/**
	 * @return date - Fecha cuando se puso la infraccion .
	 */
	public LocalDateTime getTicketIssueDate() 
	{
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pago el que recibio la infraccion en USD.
	 */
	public double getTotalPaid() 
	{
		// TODO Auto-generated method stub
		return Double.parseDouble(totalPaid);
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() 
	{
		// TODO Auto-generated method stub
		return accidentIndicator;
	}

	/**
	 * @return description - Descripcion textual de la infraccion.
	 */
	public String  getViolationDescription() 
	{
		// TODO Auto-generated method stub
		return violationDescription;
	}
	/**
	 * Retorna el StreetId de la infraccion
	 * @returnStreetId de la infraccion
	 */
	public String getStreetSegId()
	{
		return Streetsegid;
	}
	/**
	 * Retorna el codigo de violacion de la infraccion
	 * @return codigo de violacion de la infraccion
	 */
	public String getViolationCode()
	{
		return violationCode;
	}

	/**
	 * Retorna el FineAMT(Cantidad a pagar) de la infraccion
	 * @return FineAMT de la infraccion
	 */
	public Integer getFineAMT()
	{
		return Integer.parseInt(FineAMT);
	}

	/**
	 * Retorna el Address_Id de la infraccion
	 * @return Address_Id de la infraccion
	 */
	public String getAddressId() 
	{
		return Address_Id;
	}
	/**
	 * Retorna el penalty1 de la infraccion
	 * @return penalty1 de la infraccion
	 */
	public Integer getPenalty1()
	{
		Integer rta=0;
		try
		{
		  rta=Integer.parseInt(penalty1);
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			rta=0;
		}
		return rta;
	}
	/**
	 * Retorna el penalty2 de la infraccion
	 * @return penalty2 de la infraccion
	 */
	public Integer getPenalty2()
	{
		Integer rta=0;
		try
		{
		  rta=Integer.parseInt(penalty2);
		}
		catch (Exception e) 
		{
			// TODO: handle exception
			rta=0;
		}
		return rta;
	}
	/**
	 * Retorna la latitud de la infraccion
	 * @return lat de la infraccion
	 */
	public Double getLat()
	{
		return  Double.parseDouble(lat.replace(',','.'));
	}
	/**
	 * Retorna la longitud de la infraccion
	 * @return lon de la infraccion
	 */
	public Double getLong()
	{
		return Double.parseDouble(lon.replace(',','.'));
	}

	@Override
	//NO MODIFICAR DEPENDE DE ESTE METODO COMPARE() EL A.1,A.2,A.3
	public int compareTo(VOMovingViolations o) 
	{
		// TODO Auto-generated method stub
		int rta=0;
		rta=ticketIssueDate.compareTo(o.getTicketIssueDate());
		return rta;
	}
	/**
	 * Compare to con coordenadas.
	 * @param w
	 * @return
	 */
	public int compareToCoord(VOMovingViolations w) 
	{
		// TODO Auto-generated method stub
		int rta=this.getLat().compareTo(w.getLat());
		if(rta==0)
		{
		   rta=this.getLong().compareTo(w.getLong());;	
		}
		return rta;
	}
	
	@Override
	public String toString() {
		return "VOMovingViolations [objectId()=" + getObjectId() + ",\n getLocation()=" + getLocation()
		+ ",\n getTicketIssueDate()=" + getTicketIssueDate() + ",\n getTotalPaid()=" + getTotalPaid()
		+ ",\n getAccidentIndicator()=" + getAccidentIndicator() + ",\n getViolationDescription()="
		+ getViolationDescription() + ",\n getStreetSegId()=" + getStreetSegId() + ",\n getAddressId()="
		+ getAddressId() + "]\n\n";
	}
	
}
