package GeneradorMapa;

import com.teamdev.jxmaps.ControlPosition;
import com.teamdev.jxmaps.LatLng;
import com.teamdev.jxmaps.Map;
import com.teamdev.jxmaps.MapOptions;
import com.teamdev.jxmaps.MapReadyHandler;
import com.teamdev.jxmaps.MapStatus;
import com.teamdev.jxmaps.MapTypeControlOptions;
import com.teamdev.jxmaps.Polyline;
import com.teamdev.jxmaps.PolylineOptions;
import com.teamdev.jxmaps.swing.MapView;

import model.data_structures.Arco;
import model.data_structures.ArregloDinamico;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.Queue;
import model.vo.CordVO;
import model.vo.pesoWay;

import com.teamdev.jxmaps.Circle;
import com.teamdev.jxmaps.LatLngBounds;
import com.teamdev.jxmaps.CircleOptions;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
/**
 * Clase que representa un mapa generado a partir de un grafo
 * @author nicot
 */
public class Mapa extends MapView
{
	/**
	 * Grafo a graficar en el mapa
	 */
	private GrafoNoDirigido<String, CordVO, pesoWay> mapa;
	/**
	 * Contructor de la clase
	 * @param pMapa
	 */
	public Mapa(GrafoNoDirigido<String, CordVO, pesoWay> pMapa,LatLng min,LatLng max,Boolean hacerSoloVertices, GrafoNoDirigido<String, CordVO, pesoWay> resaltar) 
	{
		mapa = pMapa;
		setOnMapReadyHandler(new MapReadyHandler() 
		{
			@Override
			public void onMapReady(MapStatus status) 
			{
				if(!hacerSoloVertices)
				{
					// Check if the map is loaded correctly
					if (status == MapStatus.MAP_STATUS_OK) 
					{
						// Getting the associated map objec
						final Map map = getMap();
						ArregloDinamico<String> vertices = mapa.keys();
						//Contador de vertices
						int n = 0;
						ArrayList<Polyline> arcos = new ArrayList<Polyline>();
						Iterator<Arco<String, CordVO, pesoWay>> colaArcos = mapa.darArcos().iterator();
						//Opciones para imprimir el mapa.
						PolylineOptions options = new PolylineOptions();
						// Setting geodesic property value
						options.setGeodesic(true);
						// Setting stroke color value
						options.setStrokeColor("#36D61C");
						// Setting stroke opacity value
						options.setStrokeOpacity(1.0);
						// Setting stroke weight value
						options.setStrokeWeight(2.0);
						// Applying options to the polyline
						CircleOptions op = new CircleOptions();
						op.setFillColor("#EC2C03");
						op.setStrokeColor("#EC2C03");
						//op.setStrokeOpacity(1.0);
						op.setRadius(2);
						ArrayList<Circle> circulos = new ArrayList<Circle>();

						while(colaArcos.hasNext()) 
						{
							Arco<String, CordVO, pesoWay> arc = colaArcos.next();
							double lat1 = mapa.getInfoVertex(arc.getInicial().getId()).getLat();
							double long1 = mapa.getInfoVertex(arc.getInicial().getId()).getLong();
							double lat2 = mapa.getInfoVertex(arc.getFinal().getId()).getLat();
							double long2 = mapa.getInfoVertex(arc.getFinal().getId()).getLong();

							if(n <2000&& lat1 >= min.getLat() && lat1 <= max.getLat() && long1 >= min.getLng() && long1 <= max.getLng()
									&& lat2 >= min.getLat() && lat2 <= max.getLat() && long2 >= min.getLng() && long2 <= max.getLng()) 
							{
								LatLng[] path = {new LatLng(lat1,long1),new LatLng(lat2,long2)};
								arcos.add(new Polyline(map));
								arcos.get(arcos.size()-1).setPath(path);
								arcos.get(arcos.size()-1).setOptions(options);
								circulos.add(new Circle(map));
								circulos.get(circulos.size()-1).setCenter(new LatLng(lat1,long1));
								circulos.get(circulos.size()-1).setOptions(op);
								circulos.add(new Circle(map));
								circulos.get(circulos.size()-1).setCenter(new LatLng(lat2,long2));
								circulos.get(circulos.size()-1).setOptions(op);
								n++;
							}
						}
						//					System.out.println("Numero de arcos graficados:: "+n);
						map.fitBounds(new LatLngBounds(min,max));
						// Setting initial zoom value
						map.setZoom(14);
					}
				}
				else
				{
					if (status == MapStatus.MAP_STATUS_OK) 
					{
						// Getting the associated map objec
						final Map map = getMap();
						ArregloDinamico<String> vertices = mapa.keys();
						//Contador de vertices
						int n = 0;
						// Applying options to the polyline
						CircleOptions op = new CircleOptions();
						op.setFillColor("#EC2C03");
						op.setStrokeColor("#EC2C03");
						//op.setStrokeOpacity(1.0);
						op.setRadius(2);
						ArrayList<Circle> circulos = new ArrayList<Circle>();

						for (int i = 0; i < vertices.darTamano(); i++) 
						{
							double lat1 = mapa.getInfoVertex(vertices.darElemento(i)).getLat();
							double long1 = mapa.getInfoVertex(vertices.darElemento(i)).getLong();

							if(n <2000&& lat1 >= min.getLat() && lat1 <= max.getLat() && long1 >= min.getLng() && long1 <= max.getLng()) 
							{
								circulos.add(new Circle(map));
								circulos.get(circulos.size()-1).setCenter(new LatLng(lat1,long1));
								circulos.get(circulos.size()-1).setOptions(op);
								n++;
							}
						}


						//					System.out.println("Numero de arcos graficados:: "+n);
						map.fitBounds(new LatLngBounds(min,max));
						// Setting initial zoom value
						map.setZoom(14);
					}
				}
				if(resaltar != null) {
					final Map map = getMap();
					ArregloDinamico<String> vertices = mapa.keys();
					//Contador de vertices
					int n = 0;
					ArrayList<Polyline> arcos = new ArrayList<Polyline>();
					Iterator<Arco<String, CordVO, pesoWay>> colaArcos = mapa.darArcos().iterator();
					//Opciones para imprimir el mapa.
					PolylineOptions options = new PolylineOptions();
					// Setting geodesic property value
					options.setGeodesic(true);
					// Setting stroke color value
					options.setStrokeColor("#140718");
					// Setting stroke opacity value
					options.setStrokeOpacity(1.0);
					// Setting stroke weight value
					options.setStrokeWeight(2.0);
					// Applying options to the polyline
					CircleOptions op = new CircleOptions();
					op.setFillColor("#00008B");
					op.setStrokeColor("#00008B");
					//op.setStrokeOpacity(1.0);
					op.setRadius(4);
					ArrayList<Circle> circulos = new ArrayList<Circle>();
					
					while(colaArcos.hasNext()) 
					{
						Arco<String, CordVO, pesoWay> arc = colaArcos.next();
						double lat1 = mapa.getInfoVertex(arc.getInicial().getId()).getLat();
						double long1 = mapa.getInfoVertex(arc.getInicial().getId()).getLong();
						double lat2 = mapa.getInfoVertex(arc.getFinal().getId()).getLat();
						double long2 = mapa.getInfoVertex(arc.getFinal().getId()).getLong();

						if(n <2000&& lat1 >= min.getLat() && lat1 <= max.getLat() && long1 >= min.getLng() && long1 <= max.getLng()
								&& lat2 >= min.getLat() && lat2 <= max.getLat() && long2 >= min.getLng() && long2 <= max.getLng()) 
						{
							LatLng[] path = {new LatLng(lat1,long1),new LatLng(lat2,long2)};
							arcos.add(new Polyline(map));
							arcos.get(arcos.size()-1).setPath(path);
							arcos.get(arcos.size()-1).setOptions(options);
							circulos.add(new Circle(map));
							circulos.get(circulos.size()-1).setCenter(new LatLng(lat1,long1));
							circulos.get(circulos.size()-1).setOptions(op);
							circulos.add(new Circle(map));
							circulos.get(circulos.size()-1).setCenter(new LatLng(lat2,long2));
							circulos.get(circulos.size()-1).setOptions(op);
							n++;
						}
					}
					//					System.out.println("Numero de arcos graficados:: "+n);
					map.fitBounds(new LatLngBounds(min,max));
					// Setting initial zoom value
					map.setZoom(14);
				}
			}
		});
	}

	/**
	 * Metodo que genera la ventana del mapa 
	 * @param sample
	 */
	public static void graficarMapa(Mapa sample)
	{
		JFrame frame = new JFrame("Washington D.C");

		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(sample, BorderLayout.CENTER);
		frame.setSize(700, 500);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}